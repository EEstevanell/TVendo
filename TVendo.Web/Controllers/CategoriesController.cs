﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data;
using TVendo.Web.Data.Entities;
using TVendo.Web.Models;

namespace TVendo.Web.Controllers
{
    [Authorize(Roles = "Developer")]
    public class CategoriesController : Controller
    {
        private readonly ICategoryRepository categoryRepository;

        public CategoriesController(ICategoryRepository categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }

        // GET: Categories
        public IActionResult Index()
        {
            return View(categoryRepository.GetAll());
        }

        // GET: Categories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = await categoryRepository.GetByIdAsync(id.Value);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // GET: Categories/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CategoryViewModel view)
        {
            if (ModelState.IsValid)
            {
                byte[] imageData  = null;
                string path = null;

                if (view.ImageFile != null && view.ImageFile.Length > 0)
                {
                    var guid = Guid.NewGuid().ToString();
                    var file = $"{guid}.svg";
                    path = file;

                    using (var ms = new MemoryStream())
                    {
                        await view.ImageFile.CopyToAsync(ms);
                        var fileBytes = ms.ToArray();
                        imageData = fileBytes;
                    }

                }

                var category = new Category { Id = view.Id, Name = view.Name };
                await categoryRepository.CreateAsync(category, imageData, path);
                return RedirectToAction(nameof(Index));
            }
            return View(view);
        }

        // GET: Categories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = await categoryRepository.GetByIdAsync(id.Value);
            if (category == null)
            {
                return NotFound();
            }
            var categoryViewModel = new CategoryViewModel { Id = category.Id, Name = category.Name, IconImageUrl = category.IconImageUrl };
            return View(categoryViewModel);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, CategoryViewModel view)
        {
            if (id != view.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                string path = null;
                byte[] imageData = null;


                if (view.ImageFile != null && view.ImageFile.Length > 0)
                {
                    var guid = Guid.NewGuid().ToString();
                    var file = $"{guid}.svg";
                    path = file;

                    using (var ms = new MemoryStream())
                    {
                        await view.ImageFile.CopyToAsync(ms);
                        var fileBytes = ms.ToArray();
                        imageData = fileBytes;
                    }
                }

                var category = new Category { IconImageUrl = view.IconImageUrl, Id = view.Id, Name = view.Name };

                try
                {
                    await categoryRepository.UpdateAsync(category, imageData, path);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await CategoryExistsAsync(category.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(view);
        }

        // GET: Categories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = await categoryRepository.GetByIdAsync(id.Value);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var category = await categoryRepository.GetByIdAsync(id);
            await categoryRepository.DeleteAsync(category);
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> CategoryExistsAsync(int id)
        {
            return await categoryRepository.ExistAsync(id);
        }
    }
}
