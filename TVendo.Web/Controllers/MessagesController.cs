﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data;
using TVendo.Web.Data.Entities;
using TVendo.Web.Data.Repositories;

namespace TVendo.Web.Controllers
{
    [Authorize(Roles = "Developer")]
    public class MessagesController : Controller
    {
        private readonly IMessageRepository messageRepository;

        public MessagesController(IMessageRepository messageRepository)
        {
            this.messageRepository = messageRepository;
        }

        // GET: Messages
        public IActionResult Index()
        {
            return View(messageRepository.GetAllWithUsers());
        }

        // GET: Messages/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = await messageRepository.GetByIdAsync(id.Value);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // GET: Messages/Create
        public IActionResult Create()
        {
            ViewData["SourceUserId"] = new SelectList(messageRepository.GetUsers(), "Id", "UserName");
            ViewData["TargetUserId"] = new SelectList(messageRepository.GetUsers(), "Id", "UserName");
            return View();
        }

        // POST: Messages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Message message)
        {
            message.SentDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                await messageRepository.CreateAsync(message);
                return RedirectToAction(nameof(Index));
            }
            ViewData["SourceUserId"] = new SelectList(messageRepository.GetUsers(), "Id", "UserName", message.SourceUserId);
            ViewData["TargetUserId"] = new SelectList(messageRepository.GetUsers(), "Id", "UserName", message.TargetUserId);
            return View(message);
        }

        // GET: Messages/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var message = await messageRepository.GetByIdAsync(id.Value);
            if (message == null)
            {
                return NotFound();
            }
            ViewData["SourceUserId"] = new SelectList(messageRepository.GetUsers(), "Id", "UserName", message.SourceUserId);
            ViewData["TargetUserId"] = new SelectList(messageRepository.GetUsers(), "Id", "UserName", message.TargetUserId);
            return View(message);
        }

        // POST: Messages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,TargetUserId,SourceUserId,Text,SentDate")] Message message)
        {
            if (id != message.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await messageRepository.UpdateAsync(message);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await MessageExistsAsync(message.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["SourceUserId"] = new SelectList(messageRepository.GetUsers(), "Id", "UserName", message.SourceUserId);
            ViewData["TargetUserId"] = new SelectList(messageRepository.GetUsers(), "Id", "UserName", message.TargetUserId);
            return View(message);
        }

        // GET: Messages/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var message = await messageRepository.GetAllWithUsers().FirstOrDefaultAsync(m => m.Id == id);
            if (message == null)
            {
                return NotFound();
            }

            return View(message);
        }

        // POST: Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var message = await messageRepository.GetByIdAsync(id);
            await messageRepository.DeleteAsync(message);
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> MessageExistsAsync(int id)
        {
            return await messageRepository.ExistAsync(id);
        }
    }
}
