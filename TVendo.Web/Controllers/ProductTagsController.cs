﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Controllers
{
    [Authorize(Roles = "Developer")]
    public class ProductTagsController : Controller
    {
        private readonly IProductTagRepository productTagRepository;

        public ProductTagsController(IProductTagRepository productTagRepository)
        {
            this.productTagRepository = productTagRepository;
        }

        // GET: ProductTags
        public async Task<IActionResult> Index()
        {
            var dataContext = productTagRepository.GetAllWithProductAndTags();
            return View(await dataContext.ToListAsync());
        }

        // GET: ProductTags/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productTag = await productTagRepository.GetAllWithProductAndTags()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productTag == null)
            {
                return NotFound();
            }

            return View(productTag);
        }

        // GET: ProductTags/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(productTagRepository.GetProducts(), "Id", "Name");
            ViewData["TagId"] = new SelectList(productTagRepository.GetTags(), "Id", "Name");
            return View();
        }

        // POST: ProductTags/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create( ProductTag productTag)
        {
            if (ModelState.IsValid)
            {
                await productTagRepository.CreateAsync(productTag);
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(productTagRepository.GetProducts(), "Id", "Name", productTag.ProductId);
            ViewData["TagId"] = new SelectList(productTagRepository.GetTags(), "Id", "Name", productTag.TagId);
            return View(productTag);
        }

        // GET: ProductTags/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productTag = await productTagRepository.GetByIdAsync(id.Value);
            if (productTag == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(productTagRepository.GetProducts(), "Id", "Name", productTag.ProductId);
            ViewData["TagId"] = new SelectList(productTagRepository.GetTags(), "Id", "Name", productTag.TagId);
            return View(productTag);
        }

        // POST: ProductTags/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,  ProductTag productTag)
        {
            if (id != productTag.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await productTagRepository.UpdateAsync(productTag);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await ProductTagExistsAsync(productTag.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(productTagRepository.GetProducts(), "Id", "Name", productTag.ProductId);
            ViewData["TagId"] = new SelectList(productTagRepository.GetTags(), "Id", "Name", productTag.TagId);
            return View(productTag);
        }

        // GET: ProductTags/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productTag = await productTagRepository.GetAllWithProductAndTags()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productTag == null)
            {
                return NotFound();
            }

            return View(productTag);
        }

        // POST: ProductTags/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var productTag = await productTagRepository.GetByIdAsync(id);
            await productTagRepository.DeleteAsync(productTag);
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> ProductTagExistsAsync(int id)
        {
            return await productTagRepository.ExistAsync(id);
        }
    }
}
