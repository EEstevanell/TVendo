﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data;

namespace TVendo.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

    public class TagIncompatibilitiesController : ControllerBase
    {
        private readonly DataContext context;

        public TagIncompatibilitiesController(DataContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetTagIncompatibilityAsync()
        {
            var dataContext = context.TagIncompatibility.Include(t => t.Tag1).Include(t => t.Tag2);
            return Ok(await dataContext.ToListAsync());
        }
    }
}