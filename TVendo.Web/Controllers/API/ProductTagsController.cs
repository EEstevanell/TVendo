﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data;

namespace TVendo.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

    public class ProductTagsController : ControllerBase
    {
        private readonly DataContext context;

        public ProductTagsController(DataContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetProductTagsAsync()
        {
            var dataContext = context.ProductTag.Include(t => t.Tag).Include(t => t.Product);
            return Ok(await dataContext.ToListAsync());
        }
    }
}