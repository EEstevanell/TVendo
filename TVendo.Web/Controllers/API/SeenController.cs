﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TVendo.Common.Models;
using TVendo.Web.Data;
using TVendo.Web.Data.Entities;
using TVendo.Web.Helpers;

namespace TVendo.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SeenController : ControllerBase
    {
        private readonly IProductRepository productRepository;
        private readonly IUserHelper userHelper;
        private readonly ISeenRepository seenRepository;

        public SeenController(IProductRepository productRepository, IUserHelper userHelper, ISeenRepository seenRepository)
        {
            this.productRepository = productRepository;
            this.userHelper = userHelper;
            this.seenRepository = seenRepository;
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> PostAsync([FromRoute] int id, [FromBody] NewUserRequest user)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState);
            }

            var product = await this.productRepository.GetByIdTrackedAsync(id);
            var realUser = await userHelper.GetUserByEmailAsync(user.Email);

            if (realUser.Id == product.UserId)
            {
                return BadRequest();
            }

            try
            {
                Seen seen = seenRepository.GetSeen(realUser, product);
                if (seen != null)
                {
                    return BadRequest(product.Id);
                }
            }
            catch (Exception)
            {
                return BadRequest();
            }

            await seenRepository.CreateAsync(new Seen()
            {
                Product = product,
                ProductId = product.Id,
                User = realUser,
                UserId = realUser.Id
            });
            product.SeenAmount++;
            await productRepository.UpdateAsync(product);
            return Ok(product.SeenAmount);
        }
    }
}