﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Extensions.Options;
using TVendo.Common.Models.NotificationHubs;
using TVendo.Web.Configuration;
using TVendo.Web.Data.Entities;
using TVendo.Web.Helpers;
using TVendo.Web.NotificationHubs;

namespace TVendo.Web.Controllers.API
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PushNotificationsController : ControllerBase
    {
        private NotificationHubClient hub;
        private readonly IUserHelper userHelper;

        public PushNotificationsController(IUserHelper userHelper)
        {
            hub = Notifications.Instance.Hub;
            this.userHelper = userHelper;
        }

        [HttpPost("register")]
        public async Task<IActionResult> CreatePushRegistrationId([FromBody] DeviceRegistration device)
        {
            try
            {
                User user = await userHelper.GetUserByEmailAsync(device.Tags[0]);
                var registrationId = device.Handle;
                var pnsToken = device.Tags[1];

                if (!string.IsNullOrEmpty(user.Handle))
                {
                    // make sure there are no existing registrations for this push handle (used for iOS and Android)
                    if (user.RegistrationID != registrationId) //Updating registration ID
                    {
                        await Notifications.Instance.DeleteAllRegistrations(user.Handle, 100);
                    }
                }

                user.Handle = pnsToken;
                user.RegistrationID = registrationId;

                var result = await userHelper.UpdateUserAsync(user);
                if (result.Succeeded)
                {
                    return Ok(user.RegistrationID);
                }
                else
                {
                    return BadRequest(result.Errors);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost("updatehandle")]
        public async Task<IActionResult> UpdateHandle([FromBody] DeviceRegistration device)
        {
            try
            {
                User user = await userHelper.GetUserByEmailAsync(device.Tags[0]);

                if (!string.IsNullOrEmpty(user.Handle))
                {
                    // make sure there are no existing registrations for this push handle (used for iOS and Android)
                    if (user.Handle != device.Handle) //Updating registration ID
                    {
                        await Notifications.Instance.DeleteAllRegistrations(user.Handle, 100);
                        user.Handle = device.Handle;
                    }
                }

                var result = await userHelper.UpdateUserAsync(user);
                if (result.Succeeded)
                {
                    return Ok(user.Handle);
                }
                else
                {
                    return BadRequest(result.Errors);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        ///
        /// <summary>
        /// Delete registration ID and unregister from receiving push notifications
        /// </summary>
        /// <param name="registrationId"></param>
        /// <returns></returns>
        [HttpPost("unregister")]
        public async Task<IActionResult> UnregisterFromNotifications([FromBody] DeviceRegistration device)
        {
            try
            {
                User user = await userHelper.GetUserByEmailAsync(device.Tags[0]);

                if (string.IsNullOrEmpty(user.RegistrationID))
                    return Ok();

                //Double check if user have another registration
                if (user.RegistrationID != device.Handle)
                    await hub.DeleteRegistrationAsync(user.RegistrationID);

                await hub.DeleteRegistrationAsync(device.Handle);

                user.Handle = null;
                user.RegistrationID = null;
                await userHelper.UpdateUserAsync(user);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        ///
        /// <summary>
        /// Register to receive push notifications
        /// </summary>
        /// <param name="id"></param>
        /// <param name="deviceUpdate"></param>
        /// <returns></returns>
        [HttpPut("enable/{id}")]
        public async Task<IActionResult> RegisterForPushNotifications(string id, [FromBody] DeviceRegistration deviceUpdate)
        {
            try
            {
                RegistrationDescription registration = null;
                switch (deviceUpdate.Platform)
                {
                    case MobilePlatform.wns:
                        registration = new WindowsRegistrationDescription(deviceUpdate.Handle);
                        break;

                    case MobilePlatform.apns:
                        registration = new AppleRegistrationDescription(deviceUpdate.Handle);
                        break;

                    case MobilePlatform.fcm:
                        registration = new FcmRegistrationDescription(deviceUpdate.Handle);
                        break;

                    default:
                        return BadRequest();
                }

                registration.RegistrationId = id;

                // add check if user is allowed to add these tags
                registration.Tags = new HashSet<string>(deviceUpdate.Tags);

                try
                {
                    await hub.CreateOrUpdateRegistrationAsync(registration);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}