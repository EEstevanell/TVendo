﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using TVendo.Common.Models;
using TVendo.Common.Models.Filter;
using TVendo.Web.Data;
using TVendo.Web.Data.Entities;
using TVendo.Web.Helpers;

namespace TVendo.Web.Controllers.API
{
    [Route("api/[controller]/[action]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class FollowController : ControllerBase
    {
        private readonly IFollowRepository followRepository;
        private IUserHelper userHelper;

        public FollowController(IFollowRepository followRepository, IUserHelper userHelper)
        {
            this.followRepository = followRepository;
            this.userHelper = userHelper;
        }

        [HttpGet]
        [ActionName("list")]
        public async Task<IActionResult> GetFollowsAsync()
        {
            var dataContext = followRepository.GetAll();
            return Ok(await dataContext.ToListAsync());
        }

        [HttpGet]
        [ActionName("GetAll")]
        public async Task<IActionResult> GetAll([FromQuery] PaginationFilter filter,[FromQuery] bool following, string email)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                if (following)
                {
                    var follows = followRepository.GetAllWithUser().Where(user => user.Follower.Email == email).Select(user => user.Followed);
                    var paginatedList = await PaginatedData<Data.Entities.User>.CreatePaginatedListAsync(
                    follows,
                    validFilter.PageNumber,
                    validFilter.PageSize);
                    return Ok(paginatedList);
                }
                else
                {
                    var follows = followRepository.GetAllWithUser().Where(user => user.Followed.Email == email).Select(user => user.Follower);
                    var paginatedList = await PaginatedData<Data.Entities.User>.CreatePaginatedListAsync(
                    follows,
                    validFilter.PageNumber,
                    validFilter.PageSize);
                    return Ok(paginatedList);
                }
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [ActionName("getfollows")]
        public async Task<IActionResult> GetUserFollowsAsync([FromBody] string userEmail)
        {
            TVendo.Web.Data.Entities.User realUser = await userHelper.GetUserByEmailAsync(userEmail);
            var dataContext = followRepository.GetFollowedByUser(realUser);
            return Ok(await dataContext.ToListAsync());
        }

        [HttpPost]
        [ActionName("getfollowinfo")]
        public async Task<IActionResult> GetUserFollowInfoAsync([FromBody] Tuple<string, string> users)
        {
            (string userEmail, string otherEmail) = users;
            TVendo.Web.Data.Entities.User realUser = await userHelper.GetUserByEmailAsync(userEmail);
            TVendo.Web.Data.Entities.User realOther = await userHelper.GetUserByEmailAsync(otherEmail);

            Follow follow = followRepository.GetFollow(realUser, realOther);

            Dictionary<string, int> result = new Dictionary<string, int>();
            result["isFollowing"] = (follow == null)?0:1;
            result["followersCount"] = realOther.FollowersCount;
            result["followingCount"] = realOther.FollowingCount;
            return Ok(result);
        }

        [HttpPost]
        [ActionName("follow")]
        public async Task<IActionResult> StartFollowingAsync([FromBody] Tuple<string, string> followUsers)
        {
            try
            {
                (string followerEmail, string followedEmail) = followUsers;
                TVendo.Web.Data.Entities.User realFollower = await userHelper.GetUserByEmailAsync(followerEmail);
                TVendo.Web.Data.Entities.User realFollowed = await userHelper.GetUserByEmailAsync(followedEmail);

                Follow follow = new Follow
                {
                    Followed = realFollowed,
                    Follower = realFollower,
                    FollowedId = realFollowed.Id,
                    FollowerId = realFollower.Id,
                };

                await followRepository.CreateAsync(follow);

                realFollower.FollowingCount++;
                realFollowed.FollowersCount++;

                await userHelper.UpdateUserAsync(realFollower);
                await userHelper.UpdateUserAsync(realFollowed);

                Dictionary<string, int> result = new Dictionary<string, int>();
                result["followersCount"] = realFollowed.FollowersCount;
                result["followingCount"] = realFollowed.FollowingCount;
                //TODO: notify that followed user is being followed by the new follower

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [ActionName("unfollow")]
        public async Task<IActionResult> StopFollowingAsync([FromBody] Tuple<string, string> followUsers)
        {
            (string followerEmail, string followedEmail) = followUsers;
            TVendo.Web.Data.Entities.User realFollower = await userHelper.GetUserByEmailAsync(followerEmail);
            TVendo.Web.Data.Entities.User realFollowed = await userHelper.GetUserByEmailAsync(followedEmail);

            Follow follow = followRepository.GetFollow(realFollower, realFollowed);
            await followRepository.DeleteAsync(follow);

            realFollower.FollowingCount--;
            realFollowed.FollowersCount--;

            await userHelper.UpdateUserAsync(realFollower);
            await userHelper.UpdateUserAsync(realFollowed);

            Dictionary<string, int> result = new Dictionary<string, int>();
            result["followersCount"] = realFollowed.FollowersCount;
            result["followingCount"] = realFollowed.FollowingCount;

            return Ok(result);
        }
    }
}