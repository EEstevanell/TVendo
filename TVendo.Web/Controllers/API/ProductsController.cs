﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TVendo.Common.Models;
using TVendo.Common.Models.Filter;
using TVendo.Web.Data;
using TVendo.Web.Helpers;

namespace TVendo.Web.Controllers.API
{
    [Route("api/[Controller]")]
    [ApiController]
    public class ProductsController : Controller
    {
        private readonly IProductRepository productRepository;
        private IUserHelper userHelper;

        public ProductsController(IProductRepository productRepository, IUserHelper userHelper)
        {
            this.productRepository = productRepository;
            this.userHelper = userHelper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] PaginationFilter filter, 
                                                [FromQuery] string searchPhrase, 
                                                [FromQuery] int? categoryId, 
                                                [FromQuery] string region,
                                                [FromQuery] decimal? minPrice,
                                                [FromQuery] decimal? maxPrice)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);

                var data = productRepository.GetAllWithCategoryAndUser();
                if (!string.IsNullOrEmpty(searchPhrase))
                {
                    //MakeSearch   TO DO    Impement Algorythm for more complex Search Later
                    data = data.Where(p => p.Name.Contains(searchPhrase));
                }
                if (categoryId != null)
                {
                    data = data.Where(p => p.CategoryId == categoryId.Value);
                }
                if (!string.IsNullOrEmpty(region))
                {
                    data = data.Where(p => p.User.Region == region);
                }
                if (minPrice != null)
                {
                    data = data.Where(p => p.Price > minPrice.Value);
                }
                if (maxPrice != null)
                {
                    data = data.Where(p => p.Price < maxPrice.Value);
                }
                var paginatedList = await PaginatedData<Data.Entities.Product>.CreatePaginatedListAsync(
                    data,
                    validFilter.PageNumber,
                    validFilter.PageSize);

                return Ok(paginatedList);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            try
            {
                var product = await productRepository.GetByIdAsync(id);
                return Ok(product);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("user/{username}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetFromUser([FromQuery] PaginationFilter filter, [FromRoute] string username)
        {
            try
            {
                //validate filter
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);

                //get user
                var user = await userHelper.GetUserByNameAsync(username);

                //get data
                var data = productRepository.GetAllWithCategoryFromUser(user);
                var paginatedList = await PaginatedData<Data.Entities.Product>.CreatePaginatedListAsync(
                    data,
                    validFilter.PageNumber,
                    validFilter.PageSize);

                return Ok(paginatedList);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex);
            }
            var dataContext = productRepository.GetAllWithCategoryAndUser();
            return Ok(await dataContext.ToListAsync());
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> PostProductAsync([FromBody] Product product)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState);
            }

            var user = await userHelper.GetUserByEmailAsync(product.User.Email);
            var category = await productRepository.GetCategories().FindAsync(product.CategoryId);

            if (category == null)
            {
                return this.BadRequest("Invalid Category");
            }

            if (user == null)
            {
                return this.BadRequest("Invalid user");
            }

            var entityProduct = new Data.Entities.Product
            {
                Category = category,
                CategoryId = product.CategoryId,
                Description = product.Description,
                Name = product.Name,
                Price = product.Price,
                User = user,
                UserId = user.Id,
                FixablePrice = product.FixablePrice,
                State = product.State
            };

            List<byte[]> Images = new List<byte[]>()
            {
                product.ImageArray,
                product.ImageArray2,
                product.ImageArray3,
                product.ImageArray4,
                product.ImageArray5,
            };

            var newProduct = await this.productRepository.CreateAsync(entityProduct, Images);
            return Ok(newProduct);
        }

        [HttpPost("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> PostProductAsync([FromRoute] int id, [FromBody] NewUserRequest user)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState);
            }

            var product = await this.productRepository.GetByIdTrackedAsync(id);
            var realUser = await userHelper.GetUserByEmailAsync(user.Email);

            if (realUser.Id != product.UserId)
            {
                return BadRequest();
            }

            if (await productRepository.UpdateDateFields(product))
                return Ok(product.LastPublished);
            else
                return BadRequest();
        }

        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> PutProduct([FromRoute] int id, [FromBody] Product product)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return this.BadRequest(ModelState);
                }

                if (id != product.Id)
                {
                    return BadRequest();
                }

                var oldProduct = await this.productRepository.GetByIdTrackedAsync(id);
                if (oldProduct == null)
                {
                    return this.BadRequest("Product Id don't exists.");
                }

                var Category = await productRepository.GetCategories().FindAsync(product.CategoryId);
                if (Category == null)
                {
                    return this.BadRequest("Invalid Subcategory");
                }

                List<byte[]> imagesData = new List<byte[]>
                {
                    product.ImageArray,
                    product.ImageArray2,
                    product.ImageArray3,
                    product.ImageArray4,
                    product.ImageArray5,
                };

                oldProduct.CategoryId = product.CategoryId;
                oldProduct.Category = Category;
                oldProduct.Name = product.Name;
                oldProduct.Description = product.Description;
                oldProduct.Price = product.Price;
                oldProduct.State = product.State;
                oldProduct.FixablePrice = product.FixablePrice;
                oldProduct.FullImageUrl = product.FullImageUrl;
                oldProduct.FullImageUrl1 = product.FullImageUrl1;
                oldProduct.FullImageUrl2 = product.FullImageUrl2;
                oldProduct.FullImageUrl3 = product.FullImageUrl3;
                oldProduct.FullImageUrl4 = product.FullImageUrl4;

                var updatedProduct = await this.productRepository.UpdateAsync(oldProduct, imagesData);
                return Ok(updatedProduct);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DeleteProduct([FromRoute] int id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return this.BadRequest(ModelState);
                }

                var product = await this.productRepository.GetByIdAsync(id);
                if (product == null)
                {
                    return this.NotFound();
                }

                await this.productRepository.DeleteAsync(product);
                return Ok(product);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex);
            }
        }

    }

    public class ProductSearchRequest
    {
        public string SearchText { get; set; }
        public int CategoryId { get; set; }
        public string Region { get; set; }
        public List<string> WithUsers { get; set; }
    }
}
