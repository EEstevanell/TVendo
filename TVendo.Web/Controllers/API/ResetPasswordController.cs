﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TVendo.Common.Models;
using TVendo.Common.Models.Errors;
using TVendo.Web.Helpers;

[Route("api/[Controller]")]
public class ResetPasswordController : Controller
{
    private readonly IUserHelper userHelper;
    private readonly IMailHelper mailHelper;
    private readonly IConfiguration configuration;

    public ResetPasswordController(
        IUserHelper userHelper, IConfiguration configuration,
        IMailHelper mailHelper)
    {
        this.userHelper = userHelper;
        this.mailHelper = mailHelper;
        this.configuration = configuration;
    }

    [HttpGet("{email}")]
    public async Task<IActionResult> RequestCode([FromRoute] string email)
    {
        var user = await userHelper.GetUserByEmailAsync(email);
        if (user != null)
        {
            var rand = new Random();
            Tuple<int, int, int, int> code = new Tuple<int, int, int, int>(rand.Next(1,10), rand.Next(1, 10), rand.Next(1, 10), rand.Next(1, 10));
            var mailSender = new MailHelper(configuration);
            this.mailHelper.SendMail(user.Email, "SeVende app Codigo Para Recuperar Contraseña", $"<h3>Código de Recuperación de Contraseña:</h3>" + $"<h1>{code.Item1.ToString()}{code.Item2.ToString()}{code.Item3.ToString()}{code.Item4.ToString()}</h1>");
            return Ok(code);
        }
        return BadRequest(new EditUserError()
        {
            Field = ErrorProfileField.UsernameField,
            Message = "Este email no pertenece a ningún usuario."
        });
    }

    [HttpPut]
    public async Task<IActionResult> ResetPassword([FromBody] NewUserRequest userRequest)
    {
        var user = await userHelper.GetUserByEmailAsync(userRequest.Email);
        if (user != null)
        {
            var myToken = await userHelper.GeneratePasswordResetTokenAsync(user);
            var result = await userHelper.ResetPasswordAsync(user, myToken, userRequest.Password);
            if (result.Succeeded)
            {
                return Ok(userRequest.Password);
            }
            else
            {
                return BadRequest(new EditUserError()
                {
                    Field = ErrorProfileField.Unknown,
                    Message = "Ocurrió un error inesperado, por favor inténtelo de nuevo."
                });
            }
        }
        return BadRequest(new EditUserError()
        {
            Field = ErrorProfileField.UsernameField,
            Message = "Este email no pertenece a ningún usuario."
        });
    }
}

