﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TVendo.Common.Models;
using TVendo.Common.Models.Errors;
using TVendo.Web.Helpers;

[Route("api/[Controller]")]
public class AccountController : Controller
{
    private readonly IUserHelper userHelper;
    private readonly IMailHelper mailHelper;
    private readonly IConfiguration configuration;

    public AccountController(
        IUserHelper userHelper, IConfiguration configuration,
        IMailHelper mailHelper)
    {
        this.userHelper = userHelper;
        this.mailHelper = mailHelper;
        this.configuration = configuration;
    }

    [HttpPost]
    public async Task<IActionResult> PostUser([FromBody] NewUserRequest request)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest();
        }

        var user = await userHelper.GetUserByNameAsync(request.UserName);
        if (user != null)
        {
            return BadRequest(new EditUserError()
            {
                Field = ErrorProfileField.UsernameField,
                Message = "Este nombre de usuario ya está siendo usado."
            });
        }

        user = await userHelper.GetUserByEmailAsync(request.Email);

        if (user != null)
        {
            return BadRequest(new EditUserError()
            {
                Field = ErrorProfileField.EmailField,
                Message = "Este email ya está registrado en la aplicación."
            });
        }

        

        user = new TVendo.Web.Data.Entities.User
        {
            Email = request.Email,
            UserName = request.UserName,
        };

        var result = await userHelper.AddUserAsync(user, request.Password);
        if (result != IdentityResult.Success)
        {
            return BadRequest(new EditUserError()
            {
                Field = ErrorProfileField.PasswordField,
                Message = "No se ha podido registrar el usuario, revise que su contraseña cumpla todos los requisitos."
            });
        }

        //var myToken = await this.userHelper.GenerateEmailConfirmationTokenAsync(user);
        //var tokenLink = this.Url.Action("ConfirmEmail", "Account", new
        //{
        //    userid = user.Id,
        //    token = myToken
        //}, protocol: HttpContext.Request.Scheme);

        //this.mailHelper.SendMail(request.Email, "Confirmación de Email", $"<h1>CVende-Confirmación de Email</h1>" +
        //    $"Para confirmar el registro, " +
        //    $"Por favor de click en este link:</br></br><a href = \"{tokenLink}\">Confirmar Email</a>");

        return Ok(new Response
        {
            IsSuccess = true,
            Message = "Fue Enviada una Confirmación de Email. Por favor confirme su cuenta e Inicie Sesión en la Aplicación."
        });
    }

    

    [HttpGet("{userNameOrEmail}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public async Task<IActionResult> GetUser([FromRoute] string userNameOrEmail)
    {
        if (string.IsNullOrEmpty(userNameOrEmail))
        {
            return BadRequest(new Response
            {
                IsSuccess = false,
                Message = "No se ha pedido Ningún Usuario."
            });
        }
        if (userHelper.IsUserName(userNameOrEmail))
        {
            var user = await userHelper.GetUserByNameAsync(userNameOrEmail);
            return Ok(user);
        }
        else
        {
            var user = await userHelper.GetUserByEmailAsync(userNameOrEmail);
            return Ok(user);
        }
    }

    [HttpPut]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public async Task<IActionResult> PutUser([FromBody] EditUserRequest userRequest)
    {
        //all errors must be saved in this instance
        var response = new EditUserErrorResponse();
        response.UserRequest = userRequest;

        if (!ModelState.IsValid)
        {
            response.UserUnknownError = new EditUserError
            {
                Field = ErrorProfileField.Unknown,
                Message = "No se pudo actualizar el perfil"
            };
            return BadRequest(response);
        }

        var user = await userHelper.GetUserByNameAsync(userRequest.UserName);
        if (user == null)
        {
            response.UserUnknownError = new EditUserError
            {
                Field = ErrorProfileField.Unknown,
                Message = "No se pudo actualizar el perfil"
            };
            return BadRequest(response);
        }

        //Change Profile Image
        if (userRequest.ImageArray != null && userRequest.ImageArray.Length > 0)
        {
            try
            {
                user = await userHelper.UpdateUserImage(user, userRequest.ImageArray);
                return Ok(user);
            }
            catch (Exception)
            {
                response.UserUnknownError = new EditUserError
                {
                    Field = ErrorProfileField.Unknown,
                    Message = "No se pudo actualizar la imagen de perfil"
                };
                return BadRequest(response);
            }
        }

        //Change Password
        if (!string.IsNullOrEmpty(userRequest.Password) && !string.IsNullOrEmpty(userRequest.NewPassword))
        {
            var responsePassword = await userHelper.ChangePasswordAsync(user, userRequest.Password, userRequest.NewPassword);
            if (!responsePassword.Succeeded)
            {
                response.UserPasswordError = new EditUserError
                {
                    Field = ErrorProfileField.NewPasswordField,
                    Message = "No se pudo actualizar la Contraseña"
                };
                return BadRequest(response);
            }
            else
            {
                response.UpdatedPassword = true;
            }
            var updatedUser = await userHelper.GetUserByNameAsync(user.UserName);
            return Ok(updatedUser);
        }

        //Change UserName
        if (!string.IsNullOrEmpty(userRequest.NewUserName))
        {
            try
            {
                var alreadyUser = await userHelper.GetUserByNameAsync(userRequest.NewUserName);
                if (alreadyUser != null)
                {
                    response.UserNameError = new EditUserError
                    {
                        Field = ErrorProfileField.UsernameField,
                        Message = "El nombre de usuario ya existe"
                    };
                    return BadRequest(response);
                }
                else
                {
                    await userHelper.UpdateUserNameAsync(user, userRequest.NewUserName);
                    user = await userHelper.GetUserByNameAsync(userRequest.NewUserName);
                    response.UpdatedUserName = true;
                }
            }
            catch (Exception)
            {
                response.UserNameError = new EditUserError
                {
                    Field = ErrorProfileField.UsernameField,
                    Message = "No se pudo actualizar el nombre de usuario"
                };
                return BadRequest(response);
            }

        }
        if (response.UserNameError!= null || response.UserPasswordError != null)
        {
            return BadRequest(response);
        }

        if (!string.IsNullOrEmpty(userRequest.Country))
        {
            user.Country = userRequest.Country;
        }
        if (!string.IsNullOrEmpty(userRequest.Region))
        {
            user.Region = userRequest.Region;
        }
        if (!string.IsNullOrEmpty(userRequest.Municipality))
        {
            user.Municipality = userRequest.Municipality;
        }

        var responseUpdate = await userHelper.UpdateUserAsync(user);
        if (responseUpdate.Succeeded)
        {
            var updatedUser = await userHelper.GetUserByNameAsync(user.UserName);
            return Ok(updatedUser);
        }
        else
        {
            response.UserUnknownError = new EditUserError
            {
                Field = ErrorProfileField.Unknown,
                Message = "No se ha podido actualizar la información del perfil"
            };
            return BadRequest(response);
        }
    }
}

