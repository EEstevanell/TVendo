﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data;

namespace TVendo.Web.Controllers.API
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class SubcategoriesController : ControllerBase
    {
        private readonly ISubcategoryRepository subcategoryRepository;

        public SubcategoriesController(ISubcategoryRepository subcategoryRepository)
        {
            this.subcategoryRepository = subcategoryRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetSubcategoriesAsync()
        {
            var dataContext = subcategoryRepository.GetAllWithCategories();
            return Ok(await dataContext.ToListAsync());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSubcategoriesFromCategoryAsync([FromRoute] int id)
        {
            var dataContext = subcategoryRepository.GetAllWithCategories().Where(x => x.CategoryId == id);
            return Ok(await dataContext.ToListAsync());
        }
    }
}