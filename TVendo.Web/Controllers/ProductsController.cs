﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data;
using TVendo.Web.Data.Entities;
using TVendo.Web.Models;

namespace TVendo.Web.Controllers
{
    [Authorize(Roles = "Developer")]
    public class ProductsController : Controller
    {
        
        private readonly IProductRepository productRepository;
        private readonly ICategoryRepository categoryrepository;
        private readonly ISubcategoryRepository subcategoryRepository;

        public ProductsController(IProductRepository productRepository, ICategoryRepository categoryrepository, ISubcategoryRepository subcategoryRepository)
        {
            this.productRepository = productRepository;
            this.categoryrepository = categoryrepository;
            this.subcategoryRepository = subcategoryRepository;
        }

        // GET: Products
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? categoryId, int? pageNumber, int? pageSize, decimal? lowerPrice, decimal? higherPrice)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = sortOrder == "Name" ? "name_desc" : "Name";
            ViewData["PriceSortParm"] = sortOrder == "Price" ? "price_desc" : "Price";
            ViewData["SubCategorySortParm"] = sortOrder == "SubCategory" ? "subCategory_desc" : "SubCategory";
            ViewData["UserSortParm"] = sortOrder == "User" ? "user_desc" : "User";
            var currentPageSize = 10;
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            if (pageSize != null)
            {
                currentPageSize = pageSize.Value;
            }
            ViewData["LowerPrice"] = lowerPrice ?? null;
            ViewData["HigherPrice"] = higherPrice ?? null;
            ViewData["PageSize"] = currentPageSize;
            ViewData["CurrentFilter"] = searchString;
            var categoryName = "Todas";
            if (categoryId != null)
            {
                var category = await this.categoryrepository.GetByIdAsync(categoryId.Value);
                if (category == null)
                {
                    var subcategory = await this.subcategoryRepository.GetByIdAsync(categoryId.Value);
                    if (subcategory != null)
                    {
                        categoryName = subcategory.Name;
                    }
                }
                else
                {
                    categoryName = category.Name;
                }
            }
            ViewData["CategoryName"] = categoryName;
            ViewData["CategoryId"] = categoryId ?? null;
            var products = this.productRepository.SortAndFilter(sortOrder, searchString, null, categoryId?? 0, lowerPrice ?? 0, higherPrice ?? decimal.MaxValue);
            return this.View(await PaginatedList<Product>.CreateAsync(products.AsNoTracking(), pageNumber ?? 1, currentPageSize));
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await productRepository.GetAllWithCategoryAndUser().FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // GET: Products/Create
        public IActionResult Create()
        {
            ViewData["SubcategoryId"] = new SelectList(productRepository.GetCategories(), "Id", "Name");
            ViewData["UserId"] = new SelectList(productRepository.GetUsers(), "Id", "UserName");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProductViewModel view)
        {
            if (ModelState.IsValid)
            {
                string[] paths = new string[5];
                List<byte[]> imagesData = new List<byte[]>();

                if (view.ImageFile != null && view.ImageFile.Length > 0)
                {
                    var guid = Guid.NewGuid().ToString();
                    var file = $"{guid}.jpg";
                    paths[0] = file;

                    using (var ms = new MemoryStream())
                    {
                        await view.ImageFile.CopyToAsync(ms);
                        var fileBytes = ms.ToArray();
                        imagesData.Add(fileBytes);
                    }

                    //path = Path.Combine(Directory.GetCurrentDirectory(),
                    //                    "wwwroot\\images\\products",
                    //                     file);

                    //using (var stream = new FileStream(path, FileMode.Create))
                    //{
                    //    await view.ImageFile.CopyToAsync(stream);
                    //}

                    //path = $"~/images/products/{file}";
                }

                var product = this.ToProduct(view, paths);
                await productRepository.CreateAsync(product, imagesData);
                return RedirectToAction(nameof(Index));
            }
            ViewData["SubcategoryId"] = new SelectList(productRepository.GetCategories(), "Id", "Name", view.CategoryId);
            ViewData["UserId"] = new SelectList(productRepository.GetUsers(), "Id", "UserName", view.UserId);
            return View(view);
        }

        private Product ToProduct(ProductViewModel view, string[] paths)
        {
            return new Product{ Id = view.Id,
                                ImageUrl = paths[0],
                                ImageUrl1 = paths[1],
                                ImageUrl2 = paths[2],
                                ImageUrl3 = paths[3],
                                ImageUrl4 = paths[4],
                                Price = view.Price,
                                Stock = view.Stock,
                                Name = view.Name,
                                User = view.User,
                                UserId = view.UserId,
                                Category = view.Category,
                                CategoryId = view.CategoryId};
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await productRepository.GetByIdAsync(id.Value);
            if (product == null)
            {
                return NotFound();
            }
            ViewData["SubcategoryId"] = new SelectList(productRepository.GetCategories(), "Id", "Name", product.CategoryId);
            ViewData["UserId"] = new SelectList(productRepository.GetUsers(), "Id", "UserName", product.UserId);
            var view = ToProductViewModel(product);
            return View(view);
        }

        private ProductViewModel ToProductViewModel(Product product)
        {
            return new ProductViewModel {Id = product.Id,
                                         ImageUrl = product.ImageUrl,
                                         Price = product.Price,
                                         Stock = product.Stock,
                                         User = product.User,
                                         UserId = product.UserId,
                                         Name = product.Name,
                                         Category = product.Category,
                                         CategoryId = product.CategoryId};
    }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ProductViewModel view)
        {
            if (id != view.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                string[] paths = new string[] 
                { 
                    view.ImageUrl,
                    view.ImageUrl1,
                    view.ImageUrl2,
                    view.ImageUrl3,
                    view.ImageUrl4
                };
                List<byte[]> imagesData = new List<byte[]>();


                if (view.ImageFile != null && view.ImageFile.Length > 0)
                {
                    var guid = Guid.NewGuid().ToString();
                    var file = $"{guid}.jpg";
                    paths[0] = file;

                    using (var ms = new MemoryStream())
                    {
                        await view.ImageFile.CopyToAsync(ms);
                        var fileBytes = ms.ToArray();
                        imagesData.Add(fileBytes);
                    }
                }

                var product = ToProduct(view, paths);

                try
                {
                    await productRepository.UpdateAsync(product);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await ProductExistsAsync(product.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["SubcategoryId"] = new SelectList(productRepository.GetCategories(), "Id", "Name", view.CategoryId);
            ViewData["UserId"] = new SelectList(productRepository.GetUsers(), "Id", "UserName", view.UserId);
            return View(view);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await productRepository.GetAllWithCategoryAndUser().FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var product = await productRepository.GetByIdAsync(id);
            await productRepository.DeleteAsync(product);
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> ProductExistsAsync(int id)
        {
            return await productRepository.ExistAsync(id);
        }
    }
}
