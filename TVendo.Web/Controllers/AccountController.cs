﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TVendo.Common.Models.Errors;
using TVendo.Web.Data.Entities;
using TVendo.Web.Helpers;
using TVendo.Web.Hubs;
using TVendo.Web.Models;

namespace TVendo.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserHelper userHelper;
        private readonly IConfiguration configuration;
        private readonly IHubContext<ChatHub> hubContext;

        private readonly IMailHelper mailHelper;

        public AccountController(IUserHelper userHelper, IConfiguration configuration, IHubContext<ChatHub> hubContext)
        {
            this.userHelper = userHelper;
            this.configuration = configuration;
            this.hubContext = hubContext;
        }
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!userHelper.IsUserName(model.EmailOrUserName))
                {
                    var user = userHelper.GetUserByEmailAsync(model.EmailOrUserName);
                    model.EmailOrUserName = user.Result.UserName;
                }
                var result = await userHelper.LoginAsync(model);
                if (result.Succeeded)
                {
                    if (Request.Query.Keys.Contains("ReturnUrl"))
                    {
                        return Redirect(Request.Query["ReturnUrl"].First());
                    }

                    return RedirectToAction("Index", "Home");
                }
            }
            ModelState.AddModelError(string.Empty, "Failed to Login.");
            return View(model);
        }
        public async Task<IActionResult> Logout()
        {
            await userHelper.LogoutAsync();
            return RedirectToAction("Index", "Home");
        }
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await userHelper.GetUserByEmailAsync(model.Email);
                if (user == null)
                {
                    user = new User
                    {
                        UserName = model.UserName,
                        Email = model.Email,
                    };
                    var result = await userHelper.AddUserAsync(user, model.Password);
                    if (result != IdentityResult.Success)
                    {
                        ModelState.AddModelError(string.Empty, "The user couldn't be created");
                        return View(model);
                    }
                    var myToken = await userHelper.GenerateEmailConfirmationTokenAsync(user);
                    var tokenLink = Url.Action("ConfirmEmail", "Account", new
                    {
                        userid = user.Id,
                        token = myToken
                    }, protocol: HttpContext.Request.Scheme);
                    //this.mailHelper.SendMail(model.Email, "CVende app Confirmación de Email", $"<h1>Confirmación de Email</h1>" + $"Para terminar el registro,"
                    //    + $"por favor de click en este link:</br></br><a href= \"{tokenLink}\">Confirmar Email</a>");
                    ViewBag.Message = "Las instrucciones para terminar el registro han sido enviadas a su email";

                    var loginViewModel = new LoginViewModel
                    {
                        Password = model.Password,
                        RememberMe = false,
                        EmailOrUserName = model.UserName
                    };
                    var loginResult = await userHelper.LoginAsync(loginViewModel);
                    if (loginResult.Succeeded)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    ModelState.AddModelError(string.Empty, "The user couldn't be login.");
                    return View();
                }
                ModelState.AddModelError(string.Empty, "This Email is already Registered");
            }
            return View(model);
        }
        public async Task<IActionResult> ChangeUser()
        {
            var user = await userHelper.GetUserByNameAsync(User.Identity.Name);
            var model = new ChangeUserViewModel();
            if (user != null)
            {
                model.UserName = user.UserName;
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChangeUser(ChangeUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!userHelper.IsUserName(model.UserName))
                {
                    ModelState.AddModelError(string.Empty, "User Name can not contain @ character.");
                    return View(model);
                }
                var user = await userHelper.GetUserByNameAsync(User.Identity.Name);
                if (user != null)
                {
                    user.UserName = model.UserName;
                    var response = await userHelper.UpdateUserAsync(user);
                    if (response.Succeeded)
                    {
                        ViewBag.UserMessage = "User updated.";
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, response.Errors.FirstOrDefault().Description);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "User not found");
                }
            }
            return View(model);
        }
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await userHelper.GetUserByNameAsync(User.Identity.Name);
                if (user != null)
                {
                    var result = await userHelper.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        ViewBag.UserMessage = "Password changed.";  //Fix Message don't show! Need to use Session.
                        return RedirectToAction("ChangeUser");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, result.Errors.FirstOrDefault().Description);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "User not found.");
                }
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateToken([FromBody] Common.Models.TokenRequest model)
        {
            if (ModelState.IsValid)
            {
                User user;
                if (userHelper.IsUserName(model.EmailOrUserName))
                {
                    user = await userHelper.GetUserByNameAsync(model.EmailOrUserName);
                }
                else
                {
                    user = await userHelper.GetUserByEmailAsync(model.EmailOrUserName);
                }

                if (user != null)
                {
                    if (model.DeviceID != null) //Check for web access
                    {
                        if (user.DeviceID == null) //First user activity
                        {
                            user.DeviceID = model.DeviceID;
                            await userHelper.UpdateUserAsync(user);
                        }
                        else if (model.DeviceID != user.DeviceID)
                        {
                            //User has changed device
                            user.DeviceID = model.DeviceID;
                            await userHelper.UpdateUserAsync(user);
                        }
                    }

                    var result = await userHelper.ValidatePasswordAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        var claims = new[]
                        {
                            new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        };
                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Tokens:Key"]));
                        var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                        var token = new JwtSecurityToken(
                            configuration["Tokens:Issuer"],
                            configuration["Tokens:Audience"],
                            claims,
                            expires: DateTime.UtcNow.AddDays(30),
                            signingCredentials: credentials);
                        var results = new
                        {
                            token = new JwtSecurityTokenHandler().WriteToken(token),
                            expiration = token.ValidTo
                        };
                        return Created(string.Empty, results);
                    }
                    return BadRequest(new LoginErrorResponse()
                        {
                            Field = ErrorField.PasswordField,
                            Message = "Contraseña incorrecta"
                        });
                }
                return BadRequest(new LoginErrorResponse()
                {
                    Field = ErrorField.UsernameField,
                    Message = "Nombre o email de usuario incorrecto"
                });
            }
            return BadRequest();
        }
        public IActionResult NotAuthorized()
        {
            return View();
        }
        public async Task<IActionResult> ConfirmEmail(string userId, string token)
        {
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(token))
            {
                return NotFound();
            }

            var user = await userHelper.GetUserByIdAsync(userId);
            if (user == null)
            {
                return NotFound();
            }
            var result = await userHelper.ConfirmEmailAsync(user, token);
            if (!result.Succeeded)
            {
                return NotFound();
            }
            return View();
        }
        public IActionResult RecoverPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> RecoverPassword(RecoverPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user;
                if (userHelper.IsUserName(model.EmailOrUserName))
                {
                    user = await userHelper.GetUserByNameAsync(model.EmailOrUserName);
                }
                else
                {
                    user = await userHelper.GetUserByEmailAsync(model.EmailOrUserName);
                }
                if (user == null)
                {
                    ModelState.AddModelError(string.Empty, "The Email or UserName doesn't correspond to a registered user");
                    return View(model);
                }

                var myToken = await userHelper.GeneratePasswordResetTokenAsync(user);
                var link = Url.Action("ResetPassword", "Account", new { token = myToken }, protocol: HttpContext.Request.Scheme);
                //var mailSender = new MailHelper(configuration);
                //this.mailHelper.SendMail(user.Email, "CVende app Resetear Contraseña", $"<h1>Recuperación de Contraseña</h1>" + $"Para resetear la contraseña,"
                //        + $"por favor de click en este link:</br></br><a href= \"{link}\">Resetear Password</a>");
                ViewBag.Message = "Las instrucciones para recuperar la contraseña han sido enviadas a su email";
                return View();
            }
            return View(model);
        }
        public IActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            User user;
            if (userHelper.IsUserName(model.EmailOrUserName))
            {
                user = await userHelper.GetUserByNameAsync(model.EmailOrUserName);
            }
            else
            {
                user = await userHelper.GetUserByEmailAsync(model.EmailOrUserName);
            }
            if (user != null)
            {
                var result = await userHelper.ResetPasswordAsync(user, model.Token, model.Password);
                if (result.Succeeded)
                {
                    ViewBag.Message = "Contraseña Reseteada Correctamente";
                    return View();
                }

                ViewBag.Message = "Ocurrió un problema mientras se intentaba resetear la contraseña";
                return View(model);
            }

            ViewBag.Message = "Usuario no encontrado";
            return View(model);
        }

        [Authorize(Roles = "Admin, Developer")]
        public async Task<IActionResult> Index()
        {
            var users = await userHelper.GetAllUserAsync();
            foreach (var user in users)
            {
                user.IsAdmin = await userHelper.IsUserInRoleAsync(user, "Admin");
            }
            return View(users);
        }

    }
}
