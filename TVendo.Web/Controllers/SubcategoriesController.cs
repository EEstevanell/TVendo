﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Controllers
{
    [Authorize(Roles = "Developer")]
    public class SubcategoriesController : Controller
    {
        private readonly ISubcategoryRepository subcategoryRepository;

        public SubcategoriesController(ISubcategoryRepository subcategoryRepository)
        {
            this.subcategoryRepository = subcategoryRepository;
        }

        // GET: Subcategories
        public async Task<IActionResult> Index()
        {
            var dataContext = subcategoryRepository.GetAllWithCategories();
            return View(await dataContext.ToListAsync());
        }

        // GET: Subcategories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var subcategory = await subcategoryRepository.GetAllWithCategories().FirstOrDefaultAsync(m => m.Id == id);
            if (subcategory == null)
            {
                return NotFound();
            }

            return View(subcategory);
        }

        // GET: Subcategories/Create
        public IActionResult Create()
        {
            ViewData["CategoryId"] = new SelectList(subcategoryRepository.GetCategories(), "Id", "Name");
            return View();
        }

        // POST: Subcategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Subcategory subcategory)
        {
            if (ModelState.IsValid)
            {
                await subcategoryRepository.CreateAsync(subcategory);
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(subcategoryRepository.GetCategories(), "Id", "Name", subcategory.CategoryId);
            return View(subcategory);
        }

        // GET: Subcategories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subcategory = await subcategoryRepository.GetByIdAsync(id.Value);
            if (subcategory == null)
            {
                return NotFound();
            }
            ViewData["CategoryId"] = new SelectList(subcategoryRepository.GetCategories(), "Id", "Name", subcategory.CategoryId);
            return View(subcategory);
        }

        // POST: Subcategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CategoryId,Name")] Subcategory subcategory)
        {
            if (id != subcategory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await subcategoryRepository.UpdateAsync(subcategory);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await SubcategoryExistsAsync(subcategory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(subcategoryRepository.GetCategories(), "Id", "Name", subcategory.CategoryId);
            return View(subcategory);
        }

        // GET: Subcategories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subcategory = await subcategoryRepository.GetAllWithCategories().FirstOrDefaultAsync(m => m.Id == id);
            if (subcategory == null)
            {
                return NotFound();
            }

            return View(subcategory);
        }

        // POST: Subcategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var subcategory = await subcategoryRepository.GetByIdAsync(id);
            await subcategoryRepository.DeleteAsync(subcategory);
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> SubcategoryExistsAsync(int id)
        {
            return await subcategoryRepository.ExistAsync(id);
        }
    }
}
