﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Controllers
{
    [Authorize(Roles = "Developer")]
    public class TagIncompatibilitiesController : Controller
    {
        private readonly ITagIncompatibilityRepository tagIncompatibilityRepository;

        public TagIncompatibilitiesController(ITagIncompatibilityRepository tagIncompatibilityRepository)
        {
            this.tagIncompatibilityRepository = tagIncompatibilityRepository;
        }

        // GET: TagIncompatibilities
        public async Task<IActionResult> Index()
        {
            var dataContext = tagIncompatibilityRepository.GetAllWithTags();
            return View(await dataContext.ToListAsync());
        }

        // GET: TagIncompatibilities/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tagIncompatibility = await tagIncompatibilityRepository.GetAllWithTags().FirstOrDefaultAsync(m => m.Id == id);
            if (tagIncompatibility == null)
            {
                return NotFound();
            }

            return View(tagIncompatibility);
        }

        // GET: TagIncompatibilities/Create
        public IActionResult Create()
        {
            ViewData["Tag1Id"] = new SelectList(tagIncompatibilityRepository.GetTags(), "Id", "Name");
            ViewData["Tag2Id"] = new SelectList(tagIncompatibilityRepository.GetTags(), "Id", "Name");
            return View();
        }

        // POST: TagIncompatibilities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Tag1Id,Tag2Id")] TagIncompatibility tagIncompatibility)
        {
            if (ModelState.IsValid)
            {
                await tagIncompatibilityRepository.CreateAsync(tagIncompatibility);
                return RedirectToAction(nameof(Index));
            }
            ViewData["Tag1Id"] = new SelectList(tagIncompatibilityRepository.GetTags(), "Id", "Name", tagIncompatibility.Tag1Id);
            ViewData["Tag2Id"] = new SelectList(tagIncompatibilityRepository.GetTags(), "Id", "Name", tagIncompatibility.Tag2Id);
            return View(tagIncompatibility);
        }

        // GET: TagIncompatibilities/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tagIncompatibility = await tagIncompatibilityRepository.GetByIdAsync(id.Value);
            if (tagIncompatibility == null)
            {
                return NotFound();
            }
            ViewData["Tag1Id"] = new SelectList(tagIncompatibilityRepository.GetTags(), "Id", "Name", tagIncompatibility.Tag1Id);
            ViewData["Tag2Id"] = new SelectList(tagIncompatibilityRepository.GetTags(), "Id", "Name", tagIncompatibility.Tag2Id);
            return View(tagIncompatibility);
        }

        // POST: TagIncompatibilities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, TagIncompatibility tagIncompatibility)
        {
            if (id != tagIncompatibility.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await tagIncompatibilityRepository.UpdateAsync(tagIncompatibility);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await TagIncompatibilityExistsAsync(tagIncompatibility.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Tag1Id"] = new SelectList(tagIncompatibilityRepository.GetTags(), "Id", "Name", tagIncompatibility.Tag1Id);
            ViewData["Tag2Id"] = new SelectList(tagIncompatibilityRepository.GetTags(), "Id", "Name", tagIncompatibility.Tag2Id);
            return View(tagIncompatibility);
        }

        // GET: TagIncompatibilities/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tagIncompatibility = await tagIncompatibilityRepository.GetAllWithTags().FirstOrDefaultAsync(m => m.Id == id);
            if (tagIncompatibility == null)
            {
                return NotFound();
            }

            return View(tagIncompatibility);
        }

        // POST: TagIncompatibilities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tagIncompatibility = await tagIncompatibilityRepository.GetByIdAsync(id);
            await tagIncompatibilityRepository.DeleteAsync(tagIncompatibility);
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> TagIncompatibilityExistsAsync(int id)
        {
            return await tagIncompatibilityRepository.ExistAsync(id);
        }
    }
}
