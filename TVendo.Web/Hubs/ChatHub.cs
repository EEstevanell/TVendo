﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVendo.Common.Models;
using TVendo.Common.Models.NotificationHubs;
using TVendo.Web.Configuration;
using TVendo.Web.Data;
using TVendo.Web.Data.Repositories;
using TVendo.Web.Helpers;
using TVendo.Web.NotificationHubs;
using Notification = TVendo.Common.Models.NotificationHubs.Notification;

namespace TVendo.Web.Hubs
{
    public class ConnectionMapping<T>
    {
        private readonly Dictionary<T, HashSet<string>> _connections = new Dictionary<T, HashSet<string>>();

        public int Count => _connections.Count;

        public void Add(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    connections = new HashSet<string>();
                    _connections.Add(key, connections);
                }

                lock (connections)
                {
                    connections.Add(connectionId);
                }
            }
        }

        public IEnumerable<string> GetConnections(T key)
        {
            HashSet<string> connections;
            if (_connections.TryGetValue(key, out connections))
            {
                return connections;
            }

            return Enumerable.Empty<string>();
        }

        public void Remove(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    return;
                }

                lock (connections)
                {
                    connections.Remove(connectionId);

                    if (connections.Count == 0)
                    {
                        _connections.Remove(key);
                    }
                }
            }
        }
    }

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ChatHub : Hub
    {
        #region Members

        private static readonly ConnectionMapping<string> _connections = new ConnectionMapping<string>();
        private static readonly MessageQueue<string, Message> messageQueue = new MessageQueue<string, Message>(1000);
        private static readonly MessageQueue<string, long> confirmationQueue = new MessageQueue<string, long>(1000);
        private static readonly ConcurrentDictionary<int, int> messageToConversation = new ConcurrentDictionary<int, int>();

        private readonly IUserHelper userHelper;
        private readonly IConversationRepository conversationRepository;
        private readonly IProductRepository productRepository;
        private readonly IOptions<NotificationHubConfiguration> standardNotificationHubConfiguration;
        private readonly IMessageRepository messageRepository;
        private readonly NotificationHubProxy _notificationHubProxy;

        #endregion Members

        #region Constructor

        public ChatHub(IUserHelper userHelper, IConversationRepository conversationRepository, IProductRepository productRepository, IOptions<NotificationHubConfiguration> standardNotificationHubConfiguration, IMessageRepository messageRepository) : base()
        {
            this.userHelper = userHelper;
            this.conversationRepository = conversationRepository;
            this.productRepository = productRepository;
            this.standardNotificationHubConfiguration = standardNotificationHubConfiguration;
            this.messageRepository = messageRepository;
            _notificationHubProxy = new NotificationHubProxy(standardNotificationHubConfiguration.Value);
        }

        #endregion Constructor

        #region Messaging Methods

        public async Task<long[]> SendMessage(Message message, Conversation conversation)
        {
            try
            {
                var targetUserEmail = message.TargetUserEmail;

                //Save conversation in DB
                var buyer = await userHelper.GetUserByEmailAsync(conversation.BuyerEmail);
                var owner = await userHelper.GetUserByEmailAsync(conversation.OwnerEmail);
                var targetUser = (targetUserEmail == buyer.Email) ? buyer : owner;
                var fromUser = (targetUserEmail == buyer.Email) ? owner : buyer;

                Data.Entities.Conversation onlineConversation;
                if (!conversation.RecordedOnline)
                {
                    try
                    {
                        onlineConversation = await conversationRepository.CreateConversation(owner, buyer, conversation.ProductId);
                    }
                    catch (Exception)
                    {
                        onlineConversation = await conversationRepository.GetConversation(owner, buyer, conversation.ProductId);
                    }
                    conversation.OnlineId = onlineConversation.Id;
                    conversation.RecordedOnline = true;
                }
                else
                {
                    onlineConversation = conversationRepository.GetConversation((int)conversation.OnlineId);
                }

                //Update message Id
                var onlineMessage = await messageRepository.CreateFromMessage(message, fromUser, targetUser, onlineConversation);
                message.OnlineId = onlineMessage.Id;
                message.SentDate = DateTime.UtcNow.ToBinary();

                messageToConversation.TryAdd(onlineMessage.Id, onlineConversation.Id);

                //Send notification
                string messageContent = JsonConvert.SerializeObject(message);
                string conversationContent = JsonConvert.SerializeObject(conversation);

                var content = $"{{ \"data\" : {JsonConvert.SerializeObject(new { message = messageContent, conversation = conversationContent })} }}";
                Notification newNotification = new Notification()
                {
                    Content = content, //"{ \"data\" : {\"message\":\"" + "From " + fromUser.UserName + ": " + message.Text + "\"}}",
                    Platform = MobilePlatform.fcm,
                    Handle = targetUser.RegistrationID,
                    Tags = new string[] { $"user_{targetUser.UserName.ToLower()}" }
                };
                await Notifications.Instance.SendNotification(newNotification, targetUser);

                //if there are enqueued messages then wait for them to get fetched
                if (messageQueue.MessagesCount(targetUserEmail) > 0 || _connections.GetConnections(targetUserEmail).Count() == 0)
                {
                    messageQueue.TryEnqueue(targetUserEmail, message);
                    return new long[] { message.OnlineId, conversation.OnlineId };
                }

                //if there aren't any enqueued messages then send the message as usual
                foreach (var connectionId in _connections.GetConnections(targetUserEmail))
                {
                    await Clients.Client(connectionId).SendAsync("ReceiveMessage", message, conversation);
                }
                return new long[] { message.OnlineId, conversation.OnlineId };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SendQueuedMessage(Message message)
        {
            string targetEmail = message.TargetUserEmail;

            Data.Entities.Conversation conversation = conversationRepository.GetConversation(messageToConversation[(int)message.OnlineId]);
            Data.Entities.User owner = await userHelper.GetUserByIdAsync(conversation.OwnerId);
            Data.Entities.User buyer = await userHelper.GetUserByIdAsync(conversation.BuyerId);
            Data.Entities.Product product = await productRepository.GetByIdTrackedAsync(conversation.ProductId);

            //get conversation data
            Conversation localConversation = conversationRepository.ToConversation(conversation, product, owner, buyer);
            foreach (string connectionId in _connections.GetConnections(targetEmail))
            {
                await Clients.Client(connectionId).SendAsync("ReceiveMessage", message, localConversation);
            }
        }

        public async Task SendQueuedAck(string userEmail, long id)
        {
            foreach (var connectionId in _connections.GetConnections(userEmail))
            {
                await Clients.Client(connectionId).SendAsync("ConfirmedDelivery", id);
            }
        }

        public async Task CheckQueuedMessages(string userEmail)
        {
            Message message;
            bool enqueued = messageQueue.TryPeek(userEmail, out message);
            if (enqueued)
            {
                await SendQueuedMessage(message);
                //messageQueue.TryDequeue(userEmail, out message);
                //enqueued = messageQueue.TryPeek(userEmail, out message);
            }
        }

        public async Task CheckAckMessages(string userEmail)
        {
            long messageId;
            bool enqueued = confirmationQueue.TryPeek(userEmail, out messageId);
            while (enqueued)
            {
                await SendQueuedAck(userEmail, messageId);
                //confirmationQueue.TryDequeue(userEmail, out messageId);
                //enqueued = confirmationQueue.TryPeek(userEmail, out messageId);
            }
        }

        public async Task<bool> AckDelivery()
        {
            try
            {
                string email = Context.UserIdentifier;
                Message message;

                if (messageQueue.TryDequeue(email, out message))
                {
                    int onlineConversationId;
                    messageToConversation.Remove((int)message.OnlineId, out onlineConversationId);
                    confirmationQueue.TryEnqueue(message.SourceUserEmail, message.Id);

                    try
                    {
                        var onlineMessage = await messageRepository.GetByIdTrackedAsync((int)message.OnlineId);
                        onlineMessage.IsDelivered = true;
                        await messageRepository.UpdateAsync(onlineMessage);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                await CheckQueuedMessages(email);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task AckAckDelivery(long id)
        {
            string email = Context.UserIdentifier;
            confirmationQueue.TryDequeue(email, out id);
            await CheckAckMessages(email);
        }

        public async Task SendMessageToAll(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        public async Task SendMessageToAllWithId(string user, string message, int id)
        {
            await Clients.All.SendAsync("ReceiveMessageWithId", user, message, id);
        }

        #endregion Messaging Methods

        #region Events

        public async Task SendLogoutRequest(string userEmail, string newDeviceID, bool isDataReset)
        {
            try
            {
                foreach (var connectionId in _connections.GetConnections(userEmail))
                {
                    await Clients.Client(connectionId).SendAsync("LogoutRequest", newDeviceID, isDataReset);
                }
            }
            catch (Exception)
            { }
        }

        public async Task<bool> CheckIfValidLogin(string currentDeviceID)
        {
            try
            {
                if (await userHelper.GetUserByEmailAsync(Context.UserIdentifier) == null)
                {
                    await SendLogoutRequest(Context.UserIdentifier, null, true);
                    return true;
                }

                var user = await userHelper.GetUserByEmailAsync(Context.UserIdentifier);
                if (user.DeviceID == currentDeviceID)
                {
                    //Force Logout of other devices of same user
                    await SendLogoutRequest(user.Email, user.DeviceID, false);
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override Task OnConnectedAsync()
        {
            string email = Context.UserIdentifier;
            _connections.Add(email, Context.ConnectionId);
            //CheckQueuedMessages(email);
            //CheckAckMessages(email);
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            string email = Context.UserIdentifier;
            _connections.Remove(email, Context.ConnectionId);

            return base.OnDisconnectedAsync(exception); ;
        }

        #endregion Events
    }
}