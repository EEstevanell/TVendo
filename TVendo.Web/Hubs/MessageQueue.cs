﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Hubs
{
    /// <summary>
    /// Wraps a queue (max size provided at constructor) for each connection provided
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Q"></typeparam>
    public class MessageQueue<T, Q>
    {
        private readonly Dictionary<T, Queue<Q>> _connectionMessages = new Dictionary<T, Queue<Q>>();
        private readonly int _maxQueueCount;
        
        public MessageQueue(int maxQueueCount)
        {
            _maxQueueCount = maxQueueCount;
        }

        public int Count
        {
            get
            {
                return _connectionMessages.Count;
            }
        }

        public int MessagesCount(T key)
        {
            lock (_connectionMessages)
            {
                Queue<Q> queue;
                if (_connectionMessages.TryGetValue(key, out queue))
                    return queue.Count;
                return 0;
            }
        }
        public bool TryEnqueue(T key, Q value)
        {
            lock (_connectionMessages)
            {
                Queue<Q> messageQueue;
                if (!_connectionMessages.TryGetValue(key, out messageQueue))
                {
                    messageQueue = new Queue<Q>();
                    _connectionMessages.Add(key, messageQueue);
                }

                lock (messageQueue)  
                {
                    if (MessagesCount(key) < _maxQueueCount)
                    {
                        messageQueue.Enqueue(value);
                        return true;
                    }
                    return false;
                }
            }
        }
        public bool TryPeek(T key, out Q value)
        {
            lock (_connectionMessages)
            {
                Queue<Q> messageQueue;
                if (!_connectionMessages.TryGetValue(key, out messageQueue))
                {
                    value = default;
                    return false;
                }

                lock (messageQueue)
                {
                    if (messageQueue.TryPeek(out value))
                        return true;
                    return false;
                }
            }
        }
        public bool TryDequeue(T key, out Q value)
        {
            lock (_connectionMessages)
            {
                Queue<Q> messageQueue;
                if (!_connectionMessages.TryGetValue(key, out messageQueue))
                {
                    value = default;
                    return false;
                }

                lock (messageQueue)
                {
                    if (messageQueue.TryDequeue(out value))
                        return true;
                    return false;
                }
            }
        }
    }
}
