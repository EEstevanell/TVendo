﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Helpers
{
    public class UpdatedInfo
    {
        //This will retain the information about how many
        //product actions (create, modify) have happened since
        //the server started. Usable as a checksum for clients
        // to know when their info isn't updated
        public static long AmountProductActions; 
    }
}
