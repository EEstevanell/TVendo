﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Service_Layer
{
    public class AppConfiguration
    {
        public string StorageConnection { get; set; }
        public string Container { get; set; }
    }
}
