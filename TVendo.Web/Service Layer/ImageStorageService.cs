﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Service_Layer
{
    public class ImageStorageService : IImageStorageService
    {
        private IOptions<AppConfiguration> config;

        public ImageStorageService(IOptions<AppConfiguration> config)
        {
            this.config = config;
        }

        public async Task<string> StoreImage(string filename, byte[] image)
        {
            try
            {
                string url;
                if (String.IsNullOrEmpty(filename))
                    url = GenerateFileName();
                else
                    url = GenerateFileName(filename);
                return await UploadFileToBlobAsync(url, image, "image/jpg");
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public async Task<string> StoreSvgImage(string filename, byte[] image)
        {
            try
            {
                string url;
                if (String.IsNullOrEmpty(filename))
                    url = GenerateFileName();
                else
                    url = GenerateFileName(filename);
                return await UploadFileToBlobAsync(url, image, "image/svg+xml");
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private async Task<string> UploadFileToBlobAsync(string fileName, byte[] fileData, string contentType)
        {
            try
            {
                CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(config.Value.StorageConnection);
                CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(config.Value.Container);

                if (await cloudBlobContainer.CreateIfNotExistsAsync())
                {
                    await cloudBlobContainer.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
                }

                if (fileName != null && fileData != null)
                {
                    CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(fileName);
                    cloudBlockBlob.Properties.ContentType = contentType;
                    await cloudBlockBlob.UploadFromByteArrayAsync(fileData, 0, fileData.Length);
                    return cloudBlockBlob.Uri.AbsoluteUri;
                }
                return "";
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private string GenerateFileName(string fileName)
        {
            string strFileName = string.Empty;
            string[] strName = fileName.Split('.');
            strFileName = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd") + "/" + DateTime.Now.ToUniversalTime().ToString("yyyyMMdd\\THHmmssfff") + "." + strName[strName.Length - 1];
            return strFileName;
        }

        private string GenerateFileName()
        {
            string strFileName = string.Empty;
            strFileName = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd") + "/" + DateTime.Now.ToUniversalTime().ToString("yyyyMMdd\\THHmmssfff");
            return strFileName;
        }

        public async Task<bool> DeleteImage(string url)
        {
            try
            {
                Uri uriObj = new Uri(url);
                string[] formatedBlobName = uriObj.LocalPath.Split('/');
                string containerName = formatedBlobName[1];
                string directoryName = formatedBlobName[2];
                string blobName = formatedBlobName[3];
                string strContainerName = config.Value.Container;


                CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(config.Value.StorageConnection);
                CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();

                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(strContainerName);
                CloudBlobDirectory blobDirectory = cloudBlobContainer.GetDirectoryReference(directoryName);
                CloudBlockBlob blockBlob = blobDirectory.GetBlockBlobReference(blobName);

                // delete blob from container        
                await blockBlob.DeleteAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
