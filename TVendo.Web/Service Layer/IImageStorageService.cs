﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Service_Layer
{
    public interface IImageStorageService
    {
        Task<string> StoreImage(string filename, byte[] image);
        Task<string> StoreSvgImage(string filename, byte[] image);
        Task<bool> DeleteImage(string url);


    }
}
