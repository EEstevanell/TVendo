﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Models
{
    public class CategoryViewModel : Category
    {
        [Display(Name = "Imagen")]
        public IFormFile ImageFile { get; set; }
    }
}
