﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Models
{
    public class RecoverPasswordViewModel
    {
        [Required]
        public string EmailOrUserName { get; set; }
    }
}
