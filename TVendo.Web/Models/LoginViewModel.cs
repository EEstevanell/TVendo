﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Models
{
    public class LoginViewModel
    {
        [Required]
        public string EmailOrUserName { get; set; }

        [Required]
        //[MinLength(8)]   !!!!Solve Later Double Reference
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
