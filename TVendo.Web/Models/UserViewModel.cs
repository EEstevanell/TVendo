﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Models
{
    public class UserViewModel : User
    {
        [Display(Name = "Foto de Perfil")]
        public IFormFile ImageFile { get; set; }
    }
}
