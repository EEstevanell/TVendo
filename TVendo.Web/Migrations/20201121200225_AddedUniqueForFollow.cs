﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TVendo.Web.Migrations
{
    public partial class AddedUniqueForFollow : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Follow_FollowedId",
                table: "Follow");

            migrationBuilder.CreateIndex(
                name: "IX_Follow_FollowedId_FollowerId",
                table: "Follow",
                columns: new[] { "FollowedId", "FollowerId" },
                unique: true,
                filter: "[FollowedId] IS NOT NULL AND [FollowerId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Follow_FollowedId_FollowerId",
                table: "Follow");

            migrationBuilder.CreateIndex(
                name: "IX_Follow_FollowedId",
                table: "Follow",
                column: "FollowedId");
        }
    }
}
