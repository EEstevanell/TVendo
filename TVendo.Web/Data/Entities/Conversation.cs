﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities
{
    public class Conversation : IEntity
    {
        public int Id { get; set; }

        public int AmountMessages { get; set; }

        [Display(Name = "Comprador")]
        public string BuyerId { get; set; }

        [Display(Name = "Comprador")]
        public User Buyer { get; set; }

        [Display(Name = "Dueño")]
        public string OwnerId { get; set; }

        [Display(Name = "Dueño")]
        public User Owner { get; set; }

        [Display(Name = "Producto")]
        public int ProductId { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
