﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TVendo.Common.Models;

namespace TVendo.Web.Data.Entities
{
    public class Product : IEntity
    {
        public int Id { get; set; }

        #region Base Information
        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Descripción")]
        public string Description { get; set; }

        [Display(Name = "Estado")]
        public ProductState State {get;set;}

        [Display(Name = "Precio innegociable")]
        public bool FixablePrice { get; set; }

        [Display(Name = "Precio")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public decimal Price { get; set; }

        [Display(Name = "Disponibles")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]
        public double Stock { get; set; }

        [Display(Name = "Usuario")]
        public string UserId { get; set; }

        [Display(Name = "Usuario")]
        public User User { get; set; }

        public DateTime LastPublished { get; set; }

        public DateTime LastModified { get; set; }
        #endregion

        #region Clasification
        [Display(Name = "Categoría")]
        public int CategoryId { get; set; }

        [Display(Name = "Categoría")]
        public Category Category { get; set; }
        #endregion

        #region Statitics
        public long SeenAmount { get; set; }
        public long CheckedAmount { get; set; }
        public int RepublishedAmount { get; set; }
        #endregion

        #region Images
        //images handling
        //If there are k < n images then the for each i>=n (image_i == null)
        [Display(Name = "Imagen")]
        public string ImageUrl { get; set; }

        [Display(Name = "Imagen1")]
        public string ImageUrl1 { get; set; }

        [Display(Name = "Imagen2")]
        public string ImageUrl2 { get; set; }

        [Display(Name = "Imagen3")]
        public string ImageUrl3 { get; set; }

        [Display(Name = "Imagen4")]
        public string ImageUrl4 { get; set; }

        public string FullImageUrl { get; set; }
        public string FullImageUrl1 { get; set; }
        public string FullImageUrl2 { get; set; }
        public string FullImageUrl3 { get; set; }
        public string FullImageUrl4 { get; set; }
        #endregion
    }
}
