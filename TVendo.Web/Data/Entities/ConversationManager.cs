﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities
{
    public class ConversationManager : IEntity
    {
        public int Id { get; set; }

        public int AmountMessages { get; set; }
    }
}
