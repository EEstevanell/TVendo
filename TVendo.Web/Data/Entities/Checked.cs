﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TVendo.Web.Data.Entities
{
    public class Checked: IEntity
    {
        public int Id { get; set; }

        [Display(Name = "Usuario")]
        public string UserId { get; set; }

        [Display(Name = "Usuario")]
        public User User { get; set; }

        [Display(Name = "Producto")]
        public int ProductId { get; set; }

        [Display(Name = "Producto")]
        public Product Product { get; set; }
    }
}
