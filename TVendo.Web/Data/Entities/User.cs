﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities
{
    public class User : IdentityUser
    {
        [NotMapped]
        public bool IsAdmin { get; set; } 
        public string ProfilePictureUrl { get; set; }
        public int FollowersCount { get; set; }
        public int FollowingCount { get; set; }

        public string Country { get; set; }
        public string Region { get; set; }
        public string Municipality { get; set; }

        public string DeviceID { get; set; }
        public string Handle { get; set; }
        public string RegistrationID { get; set; }

    }
}
