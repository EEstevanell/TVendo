﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities
{
    public class TagIncompatibility : IEntity
    {
        public int Id { get; set; }

        [Display(Name = "Tag 1")]
        public int Tag1Id { get; set; }

        [Display(Name = "Tag 2")]
        public int Tag2Id { get; set; }

        [Display(Name = "Tag 1")]
        public Tag Tag1 { get; set; }

        [Display(Name = "Tag 1")]
        public Tag Tag2 { get; set; }
    }
}
