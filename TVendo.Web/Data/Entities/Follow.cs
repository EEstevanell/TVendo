﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities
{
    public class Follow : IEntity
    {
        public int Id { get; set; }

        [Display(Name = "Follower")]
        public string FollowerId { get; set; }

        [Display(Name = "Followed")]
        public string FollowedId { get; set; }
        
        [Display(Name = "Follower")]
        public User Follower { get; set; }

        [Display(Name = "Followed")]
        public User Followed { get; set; }
    }
}
