﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities.Entity_Configurations
{
    public class ProductTagConfiguration : IEntityTypeConfiguration<ProductTag>
    {
        public void Configure(EntityTypeBuilder<ProductTag> builder)
        {
            //builder.HasKey(x => new { x.ProductId, x.TagId });
            builder.HasOne(x => x.Product).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Tag).WithMany().OnDelete(DeleteBehavior.Restrict);
        }
    }
}
