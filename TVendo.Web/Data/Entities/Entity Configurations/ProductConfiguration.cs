﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities.Entity_Configurations
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            //builder.HasKey(x => new { x.ProductId, x.TagId });
            builder.HasOne(x => x.User).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Category).WithMany().OnDelete(DeleteBehavior.Restrict);
        }
    }
}
