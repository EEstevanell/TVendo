﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities.Entity_Configurations
{
    public class SeenConfiguration : IEntityTypeConfiguration<Seen>
    {
        public void Configure(EntityTypeBuilder<Seen> builder)
        {
            //builder.HasKey(x => new { x.ProductId, x.TagId });
            builder.HasKey(x => new { x.ProductId, x.UserId });
        }
    }
}
