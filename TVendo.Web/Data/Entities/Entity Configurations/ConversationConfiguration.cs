﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities.Entity_Configurations
{
    public class ConversationConfiguration : IEntityTypeConfiguration<Conversation>
    {
        public void Configure(EntityTypeBuilder<Conversation> builder)
        {
            builder.HasOne(x => x.Buyer).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Owner).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.HasIndex(x => new { x.ProductId, x.OwnerId, x.BuyerId }).IsUnique();
        }
    }
}
