﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities.Entity_Configurations
{
    public class FollowConfiguration : IEntityTypeConfiguration<Follow>
    {
        public void Configure(EntityTypeBuilder<Follow> builder)
        {
            //builder.HasKey(x => new { x.Tag1Id, x.Tag2Id });
            builder.HasOne(x => x.Followed).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Follower).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.HasIndex(x => new { x.FollowedId, x.FollowerId }).IsUnique();
        }
    }
}
