﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities.Entity_Configurations
{
    public class TagIncompabilityConfiguration : IEntityTypeConfiguration<TagIncompatibility>
    {
        public void Configure(EntityTypeBuilder<TagIncompatibility> builder)
        {
            //builder.HasKey(x => new { x.Tag1Id, x.Tag2Id });
            builder.HasOne(x => x.Tag1).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Tag2).WithMany().OnDelete(DeleteBehavior.Restrict);
        }
    }
}
