﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities.Entity_Configurations
{
    public class MessageConfiguration : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            //builder.HasKey(x => new { x.ProductId, x.TagId });
            builder.HasOne(x => x.TargetUser).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.SourceUser).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Conversation).WithMany().OnDelete(DeleteBehavior.Restrict);
        }
    }
}
