﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities
{
    public class Category : IEntity
    {
        public int Id { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        public string Name { get; set; }
        
        [Display(Name = "Icono")]
        public string IconImageUrl { get; set; }
    }
}
