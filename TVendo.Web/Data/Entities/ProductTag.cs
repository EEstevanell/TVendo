﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities
{
    public class ProductTag : IEntity
    {
        public int Id { get; set; }

        [Display(Name = "Producto")]
        public int ProductId { get; set; }

        [Display(Name = "Tag")]
        public int TagId { get; set; }


        [Display(Name = "Producto")]
        public Product Product { get; set; }

        [Display(Name = "Tag")]
        public Tag Tag { get; set; }
    }
}
