﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TVendo.Web.Data.Entities
{
    public class Message : IEntity
    {
        public int Id { get; set; }

        [Display(Name = "Destinatario")]
        public string TargetUserId { get; set; }

        [Display(Name = "Conversación")]
        public Conversation Conversation { get; set; }

        [Display(Name = "Conversación")]
        public int ConversationId { get; set; }

        [Display(Name = "Destinatario")]
        public User TargetUser { get; set; }

        [Display(Name = "Usuario")]
        public string SourceUserId { get; set; }

        [Display(Name = "Usuario")]
        public User SourceUser { get; set; }

        [Display(Name = "Texto")]
        public string Text { get; set; }

        [Display(Name = "Fecha de Envío")]
        public DateTime SentDate { get; set; }

        [Display(Name = "Fecha de Entrega")]
        public DateTime? DeliveredDate { get; set; }

        [Display(Name = "Entregado")]
        public bool IsDelivered { get; set; }
    }
}
