﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data.Entities;
using TVendo.Web.Helpers;

namespace TVendo.Web.Data
{
    public class SeedDb
    {
        private readonly DataContext context;
        private readonly IUserHelper userHelper;
        private Random random;

        public SeedDb(DataContext context, IUserHelper userHelper)
        {
            this.context = context;
            this.userHelper = userHelper;
            this.random = new Random();
        }

        public async Task SeedAsync()
        {
            //await context.Database.EnsureCreatedAsync();
            try
            {
                context.Database.Migrate();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            await this.userHelper.CheckRoleAsync("Admin");
            await this.userHelper.CheckRoleAsync("Developer");

            #region User Yunior
            var userYuni = await this.userHelper.GetUserByEmailAsync("y.tejeda03@gmail.com");            
            if (userYuni == null)
            {
                userYuni = new User
                {
                    Email = "y.tejeda03@gmail.com",
                    UserName = "blondy03"
                };

                var result = await this.userHelper.AddUserAsync(userYuni, "AsusHero03!");
                if (result != IdentityResult.Success)
                {
                    throw new InvalidOperationException("Could not create the user blondy03 on seeder");
                }
                var token = await this.userHelper.GenerateEmailConfirmationTokenAsync(userYuni);
                await this.userHelper.ConfirmEmailAsync(userYuni, token);
            }

            var isInRole = await this.userHelper.IsUserInRoleAsync(userYuni, "Developer");
            if (!isInRole)
            {
                await this.userHelper.AddUserToRoleAsync(userYuni, "Developer");
            }
            #endregion

            #region User Ernesto
            var userErne = await this.userHelper.GetUserByEmailAsync("ernestoestevanellv@gmail.com");
            if (userErne == null)
            {
                userErne = new User
                {
                    Email = "ernestoestevanellv@gmail.com",
                    UserName = "EEstevanell",
                };

                var result = await this.userHelper.AddUserAsync(userErne, "erne199609");
                if (result != IdentityResult.Success)
                {
                    throw new InvalidOperationException("Could not create the user EEstevanell on seeder");
                }
                var token = await this.userHelper.GenerateEmailConfirmationTokenAsync(userErne);
                await this.userHelper.ConfirmEmailAsync(userErne, token);
            }

            isInRole = await this.userHelper.IsUserInRoleAsync(userErne, "Developer");
            if (!isInRole)
            {
                await this.userHelper.AddUserToRoleAsync(userErne, "Developer");
            }
            #endregion

            #region Categories
            if (!context.Categories.Any())
            {
                AddCategory("Celulares");
                AddCategory("Accesorios de celulares");
                AddCategory("Televisores");
                AddCategory("Electrodomésticos");
                AddCategory("Consola de videojuegos");
                AddCategory("Videojuegos");
                AddCategory("PCs");
                AddCategory("Laptops");
                AddCategory("Tablets");
                AddCategory("Equipos de Audio");
                AddCategory("Equipos de Video");
                AddCategory("Equipos de Fotografía");
                AddCategory("Autos");
                AddCategory("Ropa y calzado");
                AddCategory("Articulos del Hogar");
                AddCategory("Joyas");
                AddCategory("Herramientas");
                AddCategory("Articulos de deporte");
                AddCategory("Mascotas");
                AddCategory("Articulos de mascotas");
                AddCategory("Inmuebles");
                AddCategory("Otros");
                await this.context.SaveChangesAsync();
            }

            if (!context.Products.Any())
            {
                await AddRandomProducts(100);
                await context.SaveChangesAsync();
            }
            #endregion
        }

        private async Task AddRandomProducts(int amount)
        {
            var categories = await context.Categories.ToListAsync();
            var users = await context.Users.ToListAsync();

            for (int i = 0; i < amount; i++)
            {
                var categoryIndex = random.Next(0, categories.Count - 1);
                var userIndex = random.Next(0, users.Count - 1);
                var state = (Common.Models.ProductState) random.Next(0, 3);

                AddProduct(
                    i.ToString(),
                    categories[categoryIndex],
                    users[userIndex],
                    $"producto {i} perteneciente a la categoría {categories[categoryIndex].Name}. El dueño se llama {users[userIndex].UserName}.",
                    state);
            }
        }

        private void AddProduct(string name, Category category, User user, string description, Common.Models.ProductState state)
        {
            context.Products.Add(new Product
            {
                Name = name,
                Price = random.Next(1000),
                Category = category,
                Stock = random.Next(100),
                User = user,
                UserId = user.Id,
                Description = description,
                LastPublished = DateTime.UtcNow,
                State = state
            });
        }

        private Tag AddTag(string name)
        {
            Tag tag = new Tag
            {
                Name = name,
            };
            context.Tags.Add(tag);
            return tag;
        }

        private TagIncompatibility AddTagIncompatibility(Tag tag1, Tag tag2)
        {
            TagIncompatibility tagI = new TagIncompatibility {
                Tag1 = tag1,
                Tag2 = tag2,
                Tag1Id = tag1.Id,
                Tag2Id = tag2.Id,
            };
            context.TagIncompatibility.Add(tagI);
            return tagI;
        }

        private void AddCategory(String name)
        {
            context.Categories.Add(new Category
                {
                    Name = name
                });
        }

        private Subcategory AddSubcategory(String name, Category category)
        {
            Subcategory subcategory = new Subcategory
            {
                Name = name,
                CategoryId = category.Id,
                Category = category
            };
            context.Subcategories.Add(subcategory);
            return subcategory;
        }
    }
}
