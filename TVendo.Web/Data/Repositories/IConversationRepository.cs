﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data.Repositories
{
    public interface IConversationRepository : IGenericRepository<Conversation>
    {
        Task<Conversation> CreateConversation(User owner, User buyer, int productId);
        Task<Conversation> GetConversation(User owner, User buyer, int productId);
        Conversation GetConversation(int id);

        Common.Models.Conversation ToConversation(Conversation conversation, Product product, User owner, User buyer);

        Task<int> AddMessage();
    }
}
