﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVendo.Web.Data.Entities;
using TVendo.Web.Service_Layer;

namespace TVendo.Web.Data
{
    public class CategoryRepository: GenericRepository<Category>, ICategoryRepository
    {
        private readonly DataContext context;
        private readonly IImageStorageService imageStorageService;
        public CategoryRepository(DataContext context, IImageStorageService imageStorageService) : base(context)
        {
            this.context = context;
            this.imageStorageService = imageStorageService;
        }

        public async Task<Category> CreateAsync(Category entity, byte[] imageData, string path)
        {
            Category category = await UploadImage(entity, imageData, path);
            System.Diagnostics.Debug.WriteLine("image url:" + category.IconImageUrl);
            await context.Set<Category>().AddAsync(category);
            await SaveAllAsync();
            return entity;
        }

        public new async Task<bool> DeleteAsync(Category entity)
        {
            try
            {
                context.Set<Category>().Remove(entity);
                bool result = await SaveAllAsync();
                if (result)
                {
                    DeleteImage(entity);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<Category> UpdateAsync(Category entity, byte[] image, string path)
        {
            try
            {
                Category category = await UpdateImage(entity, image, path);
                context.Set<Category>().Update(category);
                await SaveAllAsync();
                return category;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Category> UploadImage(Category category, byte[] imageData, string path)
        {
            category.IconImageUrl = await imageStorageService.StoreSvgImage(path, imageData);
            return category;
        }
        public bool DeleteImage(Category category)
        {
            try
            {
                if (!string.IsNullOrEmpty(category.IconImageUrl))
                {
                    imageStorageService.DeleteImage(category.IconImageUrl);
                }


                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Category> UpdateImage(Category category, byte[] imageData, string path)
        {
            if (imageData != null)
            {
                if (!string.IsNullOrEmpty(category.IconImageUrl))
                {
                    await imageStorageService.DeleteImage(category.IconImageUrl);
                }
                category.IconImageUrl = await imageStorageService.StoreSvgImage(null, imageData);
            }

            return category;
        }

    }
}
