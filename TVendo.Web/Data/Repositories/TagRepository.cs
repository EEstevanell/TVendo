﻿using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data
{
    public class TagRepository : GenericRepository<Tag>, ITagRepository
    {
        public TagRepository(DataContext context) : base(context)
        {
        }
    }
}
