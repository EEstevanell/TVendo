﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data.Repositories
{
    public class SeenRepository : GenericRepository<Seen>, ISeenRepository
    {
        private readonly DataContext context;

        public SeenRepository(DataContext context) : base(context)
        {
            this.context = context;
        }

        public IQueryable<Seen> GetAllWithUserProducts()
        {
            return context.Seen.Include(x => x.User).Include(x => x.Product);
        }

        public DbSet<Product> GetProducts()
        {
            return context.Products;
        }

        public Seen GetSeen(User user, Product product)
        {
            return context.Seen.Find(product.Id, user.Id);
        }

        public IQueryable<Seen> GetSeenByProduct(Product product)
        {
            return context.Seen.Where(x => x.Product.Id == product.Id);
        }

        public IQueryable<Seen> GetSeenByUser(User user)
        {
            return context.Seen.Where(x => x.User.Id == user.Id);
        }

        public DbSet<User> GetUsers()
        {
            return context.Users;
        }
    }
}
