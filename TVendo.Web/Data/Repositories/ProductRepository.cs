﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVendo.Web.Data.Entities;
using TVendo.Web.Service_Layer;

namespace TVendo.Web.Data
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        private readonly DataContext context;
        private readonly IImageStorageService imageStorageService;

        public async Task<Product> UploadImages(Product product, List<byte[]> imagesData)
        {
            product.FullImageUrl = await imageStorageService.StoreImage(product.ImageUrl, imagesData[0]);
            if (imagesData.Count > 1 && imagesData[1] != null)
            {
                product.FullImageUrl1 = await imageStorageService.StoreImage(product.ImageUrl1, imagesData[1]);
                if (imagesData.Count > 2 && imagesData[2] != null)
                {
                    product.FullImageUrl2 = await imageStorageService.StoreImage(product.ImageUrl2, imagesData[2]);
                    if (imagesData.Count > 3 && imagesData[3] != null)
                    {
                        product.FullImageUrl3 = await imageStorageService.StoreImage(product.ImageUrl3, imagesData[3]);
                        if (imagesData.Count > 4 && imagesData[4] != null)
                        {
                            product.FullImageUrl4 = await imageStorageService.StoreImage(product.ImageUrl4, imagesData[4]);
                        }
                    }
                }
            }
            return product;
        }

        public async Task<Product> UpdateImages(Product product, List<byte[]> imagesData)
        {
            if (imagesData.Count < 5)
            {
                throw new Exception("Needed 5 images in order to update product.");
            }
            if (imagesData.Count < 1)
            {
                throw new Exception("Needed at least 1 image in order to update product.");
            }

            var oldProduct = await GetByIdAsync(product.Id);
            if (!string.IsNullOrEmpty(oldProduct.FullImageUrl) && product.FullImageUrl != oldProduct.FullImageUrl)
            {
                await imageStorageService.DeleteImage(oldProduct.FullImageUrl);
            }
            if (!string.IsNullOrEmpty(oldProduct.FullImageUrl1)
                && product.FullImageUrl != oldProduct.FullImageUrl1
                && product.FullImageUrl1 != oldProduct.FullImageUrl1)
            {
                await imageStorageService.DeleteImage(oldProduct.FullImageUrl1);
            }
            if (!string.IsNullOrEmpty(oldProduct.FullImageUrl2)
                && product.FullImageUrl != oldProduct.FullImageUrl2
                && product.FullImageUrl1 != oldProduct.FullImageUrl2
                && product.FullImageUrl2 != oldProduct.FullImageUrl2)
            {
                await imageStorageService.DeleteImage(oldProduct.FullImageUrl2);
            }
            if (!string.IsNullOrEmpty(oldProduct.FullImageUrl3)
                && product.FullImageUrl != oldProduct.FullImageUrl3
                && product.FullImageUrl1 != oldProduct.FullImageUrl3
                && product.FullImageUrl2 != oldProduct.FullImageUrl3
                && product.FullImageUrl3 != oldProduct.FullImageUrl3)
            {
                await imageStorageService.DeleteImage(oldProduct.FullImageUrl3);
            }
            if (!string.IsNullOrEmpty(oldProduct.FullImageUrl4) && product.FullImageUrl != oldProduct.FullImageUrl4 && product.FullImageUrl1 != oldProduct.FullImageUrl4 && product.FullImageUrl2 != oldProduct.FullImageUrl4 && product.FullImageUrl3 != oldProduct.FullImageUrl4 && product.FullImageUrl4 != oldProduct.FullImageUrl4)
            {
                await imageStorageService.DeleteImage(oldProduct.FullImageUrl4);
            }

            if (imagesData[0] != null)
            {
                product.FullImageUrl = await imageStorageService.StoreImage(null, imagesData[0]);
            }

            if (imagesData[1] != null)
            {
                product.FullImageUrl1 = await imageStorageService.StoreImage(null, imagesData[1]);
            }

            if (imagesData[2] != null)
            {
                product.FullImageUrl2 = await imageStorageService.StoreImage(null, imagesData[2]);
            }

            if (imagesData[3] != null)
            {
                product.FullImageUrl3 = await imageStorageService.StoreImage(null, imagesData[3]);
            }

            if (imagesData[4] != null)
            {
                product.FullImageUrl4 = await imageStorageService.StoreImage(null, imagesData[4]);
            }
            return product;
        }

        public async Task<bool> DeleteImages(Product product)
        {
            try
            {
                if (!string.IsNullOrEmpty(product.ImageUrl))
                {
                    await imageStorageService.DeleteImage(product.FullImageUrl);
                }

                if (!string.IsNullOrEmpty(product.ImageUrl1))
                {
                    await imageStorageService.DeleteImage(product.FullImageUrl1);
                }

                if (!string.IsNullOrEmpty(product.ImageUrl2))
                {
                    await imageStorageService.DeleteImage(product.FullImageUrl2);
                }

                if (!string.IsNullOrEmpty(product.ImageUrl3))
                {
                    await imageStorageService.DeleteImage(product.FullImageUrl3);
                }

                if (!string.IsNullOrEmpty(product.ImageUrl4))
                {
                    await imageStorageService.DeleteImage(product.FullImageUrl4);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProductRepository(DataContext context, IImageStorageService imageStorageService) : base(context)
        {
            this.context = context;
            this.imageStorageService = imageStorageService;
        }

        public async Task<Product> CreateAsync(Product entity, List<byte[]> imagesData)
        {
            Product product = await UploadImages(entity, imagesData);
            System.Diagnostics.Debug.WriteLine("image url:" + product.FullImageUrl);
            product.LastPublished = DateTime.UtcNow;
            await context.Set<Product>().AddAsync(product);
            await SaveAllAsync();
            return entity;
        }

        public new async Task<bool> DeleteAsync(Product entity)
        {
            try
            {
                context.Set<Product>().Remove(entity);
                bool result = await SaveAllAsync();
                if (result)
                {
                    await DeleteImages(entity);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IQueryable<Product> GetAllWithCategoryAndUser()
        {
            return context.Products
                .Include(x => x.User)
                .Include(x => x.Category)
                .OrderByDescending(x => GetPunctuation(x));
        }

        public IQueryable<Product> GetAllWithCategoryFromUser(User user)
        {
            return context
                .Products
                .Where(x => x.UserId == user.Id)
                .Include(x => x.Category)
                .OrderByDescending(x => GetPunctuation(x));
        }

        public static int GetPunctuation(Product entity)
        {
            int punctuation;
            int basePunctuation = 1500 * 60;

            //minutes count
            TimeSpan timeSpan = DateTime.UtcNow - entity.LastPublished;
            int totalMinutes = Math.Min((int)timeSpan.TotalSeconds, basePunctuation);

            //punctuation function
            punctuation = basePunctuation - totalMinutes;
            //entity.Puntuation = punctuation;
            return punctuation;
        }

        public DbSet<Category> GetCategories()
        {
            return context.Categories;
        }

        public DbSet<User> GetUsers()
        {
            return context.Users;
        }

        public IQueryable<Product> SortAndFilter(string sortOrder, string searchString, List<int> tags, int category, decimal lowerPrice, decimal higherPrice)
        {
            IQueryable<Product> products;
            if (tags != null)
            {
                var tagProducts = from t in context.ProductTag select t;
                foreach (var item in tags)
                {
                    tagProducts = tagProducts.Where(t => t.TagId == item);
                }
                products = from t in tagProducts.Include(t => t.Product) select t.Product;
                products = from p in products.Include(p => p.User).Include(p => p.Category) select p;
            }
            else
            {
                products = from p in context.Products.Include(p => p.User).Include(p => p.Category) select p;
            }
            if (category != 0)
            {
                products = products.Where(p => p.CategoryId == category || p.CategoryId == category);
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                products = products.Where(p => p.Name.Contains(searchString) ||
                                                 p.Category.Name.Contains(searchString) ||
                                                 p.User.UserName.Contains(searchString));
            }
            if (lowerPrice != 0 || higherPrice != decimal.MaxValue)
            {
                products = products.Where(p => p.Price >= lowerPrice && p.Price <= higherPrice);
            }
            switch (sortOrder)
            {
                case "name_desc":
                    {
                        products = products.OrderByDescending(p => p.Name);
                        break;
                    }
                case "Name":
                    {
                        products = products.OrderBy(p => p.Name);
                        break;
                    }
                case "Price":
                    {
                        products = products.OrderBy(p => p.Price);
                        break;
                    }
                case "price_desc":
                    {
                        products = products.OrderByDescending(p => p.Price);
                        break;
                    }
                case "Subcategory":
                    {
                        products = products.OrderBy(p => p.Category.Name);
                        break;
                    }
                case "subcategory_desc":
                    {
                        products = products.OrderByDescending(p => p.Category.Name);
                        break;
                    }
                case "User":
                    {
                        products = products.OrderBy(p => p.User.UserName);
                        break;
                    }
                case "user_desc":
                    {
                        products = products.OrderByDescending(p => p.User.UserName);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            return products;

        }

        public bool IsValidRepublishDate(Product entity)
        {
            try
            {
                if (DateTime.UtcNow <= entity.LastPublished) //check for unvalid requests
                {
                    return false;
                }

                TimeSpan timeSpan = TimeSpan.FromDays(1) - (DateTime.UtcNow - entity.LastPublished);
                return timeSpan.TotalSeconds <= 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<Product> UpdateAsync(Product entity, List<byte[]> images)
        {
            try
            {
                Product product = await UpdateImages(entity, images);
                product.LastModified = DateTime.UtcNow;
                context.Set<Product>().Update(product);
                await SaveAllAsync();
                return product;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> UpdateDateFields(Product entity)
        {
            try
            {
                if (!IsValidRepublishDate(entity))
                {
                    return false;
                }

                entity.LastPublished = DateTime.UtcNow;
                entity.LastModified = DateTime.UtcNow;
                context.Set<Product>().Update(entity);
                await SaveAllAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
