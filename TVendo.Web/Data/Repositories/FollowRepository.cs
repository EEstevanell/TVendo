﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data
{
    public class FollowRepository : GenericRepository<Follow>, IFollowRepository
    {
        private readonly DataContext context;

        public FollowRepository(DataContext context) : base(context)
        {
            this.context = context;
        }

        public IQueryable<Follow> GetAllWithUser()
        {
            return context.Follow.Include(x => x.Follower).Include(x => x.Followed);
        }

        public Follow GetFollow(User follower, User followed)
        {
            return context.Follow.Where(x => x.FollowedId == followed.Id && x.FollowerId == follower.Id).FirstOrDefault();
        }

        public IQueryable<Follow> GetFollowedByUser(User user)
        {
            return context.Follow.Include(t => t.Follower).Where(t => t.FollowerId == user.Id.ToString());
        }

        public DbSet<User> GetUsers()
        {
            return context.Users;
        }
    }
}
