﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data
{
    public class SubcategoryRepository : GenericRepository<Subcategory>, ISubcategoryRepository
    {
        private readonly DataContext context;

        public SubcategoryRepository(DataContext context) : base(context)
        {
            this.context = context;
        }

        public IQueryable<Subcategory> GetAllWithCategories()
        {
            return context.Subcategories.Include(x => x.Category);
        }

        public DbSet<Category> GetCategories()
        {
            return context.Categories;
        }
    }
}
