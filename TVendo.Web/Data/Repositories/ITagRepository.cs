﻿using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data
{
    public interface ITagRepository : IGenericRepository<Tag>
    {
    }
}
