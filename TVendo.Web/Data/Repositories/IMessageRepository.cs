﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data.Repositories
{
    public interface IMessageRepository : IGenericRepository<Message>
    {
        IQueryable<Message> GetAllWithUsers();
        DbSet<User> GetUsers();
        Task<Message> CreateFromMessage(Common.Models.Message message, User sender, User target, Conversation conversation);
        Task<List<Message>> GetMessageQueue(User target);
    }
}
