﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data
{
    public interface ITagIncompatibilityRepository:IGenericRepository<TagIncompatibility>
    {
        DbSet<Tag> GetTags();

        IQueryable<TagIncompatibility> GetAllWithTags();
    }
}
