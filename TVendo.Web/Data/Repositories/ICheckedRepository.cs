﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data
{
    public interface ICheckedRepository : IGenericRepository<Checked>
    {
        IQueryable<Checked> GetAllWithUserProducts();
        IQueryable<Checked> GetCheckedByUser(User user);
        IQueryable<Checked> GetCheckedByProduct(Product product);
        Checked GetChecked(User user, Product product);
        DbSet<User> GetUsers();
        DbSet<Product> GetProducts();
    }
}
