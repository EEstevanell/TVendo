﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data.Repositories
{
    public class CheckedRepository : GenericRepository<Checked>, ICheckedRepository
    {
        private readonly DataContext context;

        public CheckedRepository(DataContext context) : base(context)
        {
            this.context = context;
        }

        public IQueryable<Checked> GetAllWithUserProducts()
        {
            return context.Checked.Include(x => x.User).Include(x => x.Product);
        }

        public DbSet<Product> GetProducts()
        {
            return context.Products;
        }

        public Checked GetChecked(User user, Product product)
        {
            return context.Checked.Find(product.Id, user.Id);
        }

        public IQueryable<Checked> GetCheckedByProduct(Product product)
        {
            return context.Checked.Where(x => x.Product.Id == product.Id);
        }

        public IQueryable<Checked> GetCheckedByUser(User user)
        {
            return context.Checked.Where(x => x.User.Id == user.Id);
        }

        public DbSet<User> GetUsers()
        {
            return context.Users;
        }
    }
}
