﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TVendo.Common.Models;
using TVendo.Web.Data.Entities;
using Conversation = TVendo.Web.Data.Entities.Conversation;
using Product = TVendo.Web.Data.Entities.Product;
using User = TVendo.Web.Data.Entities.User;

namespace TVendo.Web.Data.Repositories
{
    public class ConversationRepository : GenericRepository<Conversation>, IConversationRepository
    {
        private readonly DataContext context;

        public ConversationRepository(DataContext context) : base(context)
        {
            this.context = context;
        }
        public async Task<Conversation> CreateConversation(User owner, User buyer, int productId)
        {
            try
            {
                Conversation newConversation = new Conversation()
                {
                    Owner = context.Users.Find(owner.Id),
                    OwnerId = owner.Id,
                    Buyer = context.Users.Find(buyer.Id),
                    BuyerId = buyer.Id,
                    ProductId = productId
                };
                await context.Set<Conversation>().AddAsync(newConversation);
                await context.SaveChangesAsync();
                return newConversation;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Conversation> GetConversation(User owner, User buyer, int productId)
        {
            return await context.Set<Conversation>().FirstOrDefaultAsync(x => x.ProductId == productId && x.Owner.Email == owner.Email && x.Buyer.Email == buyer.Email);
        }
        public Conversation GetConversation(int id)
        {
            return context.Conversations.Find(id);
        }
        public async Task<int> AddMessage()
        {
            int tries = 0;
            while (true)
            {
                try
                {
                    //Update DB conversationManager amount
                    ConversationManager conversationManager = await context.ConversationManagers.FirstAsync();
                    conversationManager.AmountMessages++;
                    context.Set<ConversationManager>().Update(conversationManager);
                    await SaveAllAsync();
                    return conversationManager.AmountMessages;
                }
                catch (DbUpdateConcurrencyException exception)
                {
                    System.Diagnostics.Debug.Write(exception);
                    Thread.Sleep(200 + 10 * tries);
                    tries++;
                    continue;
                }
            }
        }
        public Common.Models.Conversation ToConversation(Conversation conversation, Product product, User owner, User buyer)
        {
            return new Common.Models.Conversation()
            {
                OnlineId = conversation.Id,
                RecordedOnline = true,
                ProductId = conversation.ProductId,
                ProductName = product.Name,
                ProductImageUrl = product.ImageUrl,
                ProductPrice = (double) product.Price,
                BuyerName = buyer.UserName,
                BuyerEmail = buyer.Email,
                BuyerImageUrl = buyer.ProfilePictureUrl,
                OwnerName = owner.UserName,
                OwnerEmail = owner.Email,
                OwnerImageUrl = owner.ProfilePictureUrl
            };
        }
    }
}
