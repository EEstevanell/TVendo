﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        IQueryable<Product> GetAllWithCategoryAndUser();
        IQueryable<Product> GetAllWithCategoryFromUser(User user);
        DbSet<Category> GetCategories();
        DbSet<User> GetUsers();
        Task<Product> CreateAsync(Product entity, List<byte[]> imagesData);
        bool IsValidRepublishDate(Product entity);
        Task<bool> UpdateDateFields(Product entity);
        new Task<bool> DeleteAsync(Product entity);
        Task<Product> UpdateAsync(Product entity, List<byte[]> images);
        IQueryable<Product> SortAndFilter(string sortOrder, string searchString, List<int> tags, int category, decimal lowerPrice, decimal higherPrice);
    }
}
