﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data
{
    public interface ISeenRepository : IGenericRepository<Seen>
    {
        IQueryable<Seen> GetAllWithUserProducts();
        IQueryable<Seen> GetSeenByUser(User user);
        IQueryable<Seen> GetSeenByProduct(Product user);
        Seen GetSeen(User user, Product product);
        DbSet<User> GetUsers();
        DbSet<Product> GetProducts();
    }
}
