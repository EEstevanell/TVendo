﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TVendo.Common.Models;
using TVendo.Web.Data.Entities;
using TVendo.Web.Helpers;
using Message = TVendo.Web.Data.Entities.Message;

namespace TVendo.Web.Data.Repositories
{
    public class MessageRepository : GenericRepository<Message>, IMessageRepository
    {
        private readonly DataContext context;
        private readonly IUserHelper userHelper;

        public MessageRepository(DataContext context, IUserHelper userHelper) : base(context)
        {
            this.context = context;
            this.userHelper = userHelper;
        }

        public async Task<Message> CreateFromMessage(Common.Models.Message message, Entities.User sender, Entities.User target, Entities.Conversation conversation)
        {
            Message newMessage = new Message()
            {
                Conversation = conversation,
                IsDelivered = message.IsConfirmed,
                SentDate = DateTime.FromBinary(message.SentDate),
                SourceUser = sender,
                TargetUser = target,
                Text = message.Text
            };
            return await CreateAsync(newMessage);
        }

        public IQueryable<Message> GetAllWithUsers()
        {
            return context.Messages.Include(x => x.TargetUser).Include(x => x.SourceUser);
        }

        public Task<List<Message>> GetMessageQueue(Entities.User target)
        {
            throw new System.NotImplementedException();
        }

        public DbSet<Entities.User> GetUsers() => context.Users;
    }
}
