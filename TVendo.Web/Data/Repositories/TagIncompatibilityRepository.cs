﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data
{
    public class TagIncompatibilityRepository : GenericRepository<TagIncompatibility>, ITagIncompatibilityRepository
    {
        private readonly DataContext context;

        public TagIncompatibilityRepository(DataContext context) : base(context)
        {
            this.context = context;
        }

        public DbSet<Tag> GetTags()
        {
            return context.Tags;
        }

        public IQueryable<TagIncompatibility> GetAllWithTags()
        {
            return context.TagIncompatibility.Include(x => x.Tag1).Include(x => x.Tag2);
        }
    }
}
