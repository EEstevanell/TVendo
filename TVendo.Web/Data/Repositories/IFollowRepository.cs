﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data
{
    public interface IFollowRepository : IGenericRepository<Follow>
    {
        IQueryable<Follow> GetAllWithUser();
        IQueryable<Follow> GetFollowedByUser(User user);
        Follow GetFollow(User follower, User followed);
        DbSet<User> GetUsers();
    }
}
