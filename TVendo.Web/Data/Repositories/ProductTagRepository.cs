﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data
{
    public class ProductTagRepository : GenericRepository<ProductTag>, IProductTagRepository
    {
        private readonly DataContext context;

        public ProductTagRepository(DataContext context) : base(context)
        {
            this.context = context;
        }

        public IQueryable<ProductTag> GetAllWithProductAndTags()
        {
            return context.ProductTag.Include(x => x.Product).Include(x => x.Tag);
        }

        public DbSet<Product> GetProducts()
        {
            return context.Products;
        }

        public DbSet<Tag> GetTags()
        {
            return context.Tags;
        }
    }
}
