﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.Data
{
    public interface ICategoryRepository: IGenericRepository<Category>
    {
        Task<Category> CreateAsync(Category entity, byte[] imageData, string path);
        new Task<bool> DeleteAsync(Category entity);
        Task<Category> UpdateAsync(Category entity, byte[] image, string path);
    }
}
