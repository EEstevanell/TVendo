﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TVendo.Web.Data.Entities;
using TVendo.Web.Data.Entities.Entity_Configurations;

namespace TVendo.Web.Data
{
    public class DataContext : IdentityDbContext<User>
    {

        //Add new db tables like here
        public DbSet<Product> Products { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Subcategory> Subcategories { get; set; }
        public DbSet<ProductTag> ProductTag { get; set; }
        public DbSet<TagIncompatibility> TagIncompatibility { get; set; }
        public DbSet<Follow> Follow { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Conversation> Conversations { get; set; }
        public DbSet<ConversationManager> ConversationManagers { get; set; }
        public DbSet<Seen> Seen { get; set; }
        public DbSet<Checked> Checked { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new SubcategoryConfiguration());
            modelBuilder.ApplyConfiguration(new ProductTagConfiguration());
            modelBuilder.ApplyConfiguration(new TagIncompabilityConfiguration());
            modelBuilder.ApplyConfiguration(new FollowConfiguration());
            modelBuilder.ApplyConfiguration(new SeenConfiguration());
            modelBuilder.ApplyConfiguration(new CheckedConfiguration());
            modelBuilder.ApplyConfiguration(new ConversationConfiguration());
            modelBuilder.ApplyConfiguration(new MessageConfiguration());
        }
    }
}
