﻿using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.NotificationHubs.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVendo.Common.Models.NotificationHubs;
using TVendo.Web.Data.Entities;

namespace TVendo.Web.NotificationHubs
{
    public class Notifications
    {
        public static Notifications Instance = new Notifications();

        public NotificationHubClient Hub { get; set; }

        private Notifications()
        {
            Hub = NotificationHubClient.CreateClientFromConnectionString("Endpoint=sb://sevendenotifications.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=tanH4TMQaNgZbneECuVSbiC36S2DOKMI4sDahv5PVN8=",
                                                                            "SevendeNotificationHub");
        }

        public async Task DeleteAllRegistrations(string pnsHandle, int top)
        {
            var registrations = await Hub.GetRegistrationsByChannelAsync(pnsHandle, top);
            foreach (RegistrationDescription registration in registrations)
            {
                await Hub.DeleteRegistrationAsync(registration);
            }
        }

        public async Task<HubResponse<NotificationOutcome>> SendNotification(Common.Models.NotificationHubs.Notification newNotification, User user)
        {
            try
            {
                //Get target user Registration Id and update Notification
                if (string.IsNullOrEmpty(user.Handle))
                    return new HubResponse<NotificationOutcome>().SetAsFailureResponse().AddErrorMessage("Target user has no active Notification Hub registration");

                if (string.IsNullOrEmpty(user.RegistrationID))
                    return new HubResponse<NotificationOutcome>().SetAsFailureResponse().AddErrorMessage("Target user has no active Notification Hub registration");

                NotificationOutcome outcome = null;
                switch (newNotification.Platform)
                {
                    case MobilePlatform.wns:
                        if (newNotification.Tags == null)
                            outcome = await Hub.SendWindowsNativeNotificationAsync(newNotification.Content);
                        else
                            outcome = await Hub.SendWindowsNativeNotificationAsync(newNotification.Content, newNotification.Tags);
                        break;

                    case MobilePlatform.apns:
                        if (newNotification.Tags == null)
                            outcome = await Hub.SendAppleNativeNotificationAsync(newNotification.Content);
                        else
                            outcome = await Hub.SendAppleNativeNotificationAsync(newNotification.Content, newNotification.Tags);
                        break;

                    case MobilePlatform.fcm:
                        if (newNotification.Tags == null)
                            outcome = await Hub.SendFcmNativeNotificationAsync(newNotification.Content);
                        else
                            outcome = await Hub.SendFcmNativeNotificationAsync(newNotification.Content, newNotification.Tags);
                        break;
                }

                if (outcome != null)
                {
                    if (!((outcome.State == NotificationOutcomeState.Abandoned) ||
                        (outcome.State == NotificationOutcomeState.Unknown)))
                        return new HubResponse<NotificationOutcome>();
                }

                return new HubResponse<NotificationOutcome>().SetAsFailureResponse().AddErrorMessage("Notification was not sent due to issue. Please send again.");
            }
            catch (MessagingException ex)
            {
                return new HubResponse<NotificationOutcome>().SetAsFailureResponse().AddErrorMessage(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return new HubResponse<NotificationOutcome>().SetAsFailureResponse().AddErrorMessage(ex.Message);
            }
        }
    }
}