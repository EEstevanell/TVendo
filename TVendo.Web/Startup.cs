﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;
using TVendo.Web.Data;
using TVendo.Web.Data.Entities;
using TVendo.Web.Data.Repositories;
using TVendo.Web.Helpers;
using TVendo.Web.Hubs;
using TVendo.Web.Models;
using TVendo.Web.Service_Layer;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Extensions.Options;
using TVendo.Web.Configuration;

namespace TVendo.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Configuring Custom Identity
            services.AddIdentity<User, IdentityRole>(cfg =>
            {
                cfg.Tokens.AuthenticatorTokenProvider = TokenOptions.DefaultAuthenticatorProvider;
                cfg.SignIn.RequireConfirmedEmail = false; //TODO: add confirmation
                cfg.User.RequireUniqueEmail = true;
                cfg.Password.RequireDigit = false;
                cfg.Password.RequiredUniqueChars = 0;
                cfg.Password.RequiredLength = 6;
                cfg.Password.RequireLowercase = false;
                cfg.Password.RequireUppercase = false;
                cfg.Password.RequireNonAlphanumeric = false;
            })
            .AddDefaultTokenProviders()
            .AddEntityFrameworkStores<DataContext>();

            services.AddAuthentication()
                .AddCookie()
                .AddJwtBearer(cfg =>
                {
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = this.Configuration["Tokens:Issuer"],
                        ValidAudience = this.Configuration["Tokens:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(
                             Encoding.UTF8.GetBytes(this.Configuration["Tokens:Key"]))
                    };
                });

            //Db Inyection
            services.AddDbContext<DataContext>(cfg =>
            {
                cfg.UseSqlServer(Configuration.GetConnectionString("AzureDB"));
            });

            //Custom Inyections
            services.AddTransient<SeedDb>();

            services.AddScoped<IMessageRepository, MessageRepository>(); //Message Repository inyection
            services.AddScoped<IConversationRepository, ConversationRepository>(); //Conversation Repository inyection
            services.AddScoped<IProductRepository, ProductRepository>(); //Product Repository inyection
            services.AddScoped<IFollowRepository, FollowRepository>(); //Follow Repository inyection
            services.AddScoped<ISeenRepository, SeenRepository>(); //Seen Repository inyection
            services.AddScoped<ICheckedRepository, CheckedRepository>(); //Checked Repository inyection
            services.AddScoped<ITagRepository, TagRepository>(); //Tag Repository inyection
            services.AddScoped<IProductTagRepository, ProductTagRepository>(); //Tag Repository inyection
            services.AddScoped<ITagIncompatibilityRepository, TagIncompatibilityRepository>(); //Tag Repository inyection
            services.AddScoped<ICategoryRepository, CategoryRepository>(); //Tag Repository inyection
            services.AddScoped<ISubcategoryRepository, SubcategoryRepository>(); //Tag Repository inyection
            services.AddScoped<IUserHelper, UserHelper>(); //User Helper inyection
            services.AddScoped<IMailHelper, MailHelper>(); //Mail Helper inyection
            services.AddScoped<IImageStorageService, ImageStorageService>(); //Image blob Helper inyection

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.ConfigureApplicationCookie(options =>
            {
                //options.LoginPath = "/Account/NotAuthorized"; This option is about what you like
                options.AccessDeniedPath = "/Account/NotAuthorized";
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //Custom Configurations
            services.Configure<AppConfiguration>(Configuration.GetSection("MyConfig"));
            services.Configure<NotificationHubConfiguration>(Configuration.GetSection("NotificationHub"));

#if DEBUG
            //Comment for Azure  SignalR
            services.AddSignalR().AddHubOptions<ChatHub>(options =>
            {
                options.KeepAliveInterval = TimeSpan.FromSeconds(3);
                options.EnableDetailedErrors = true;
                options.HandshakeTimeout = TimeSpan.FromSeconds(5);
            });
#else
            services.AddSignalR().AddAzureSignalR().AddHubOptions<ChatHub>(options =>
            {
                options.KeepAliveInterval = TimeSpan.FromSeconds(3);
                options.EnableDetailedErrors = true;
                options.HandshakeTimeout = TimeSpan.FromSeconds(5);
            });
#endif
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

#if DEBUG
            app.UseStaticFiles(); //comment for Azure SignalR
#else
            app.UseHttpsRedirection(); //comment for offline testing
#endif
            app.UseCookiePolicy();
            app.UseAuthentication();

#if DEBUG
            //Comment for Azure SignalR
            app.UseSignalR(routes =>
            {
                routes.MapHub<ChatHub>("/chatHub");
            });

#else
            //Uncomment for Azure SignalR
            app.UseFileServer();
            app.UseAzureSignalR(routes =>
            {
                routes.MapHub<ChatHub>("/chatHub");
            });
#endif

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}